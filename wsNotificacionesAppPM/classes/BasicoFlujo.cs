﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Configuration;
using System.IO;
using System.Drawing;
using System.Net;

namespace workFlow.classes
{
    public class BasicoFlujo
    {
        public static string getListaDeFlujosUsuario(int idUsuario)
        {
            string resultado = "";
            try
            {
                DataTable flujos = DatabaseConnection.executeNonQueryDT("EXEC getDocumentosAFirmar2 " + idUsuario, CommandType.Text, ConnectionString.WorkFlow);

                List<string> listaFlujos = new List<string>();
                List<int> numElementos = new List<int>();

                foreach (DataRow dr in flujos.Rows)
                {
                    string aux = dr["DescripcionFlujo"].ToString();
                    if (listaFlujos.Contains(aux))
                    {
                        numElementos[listaFlujos.IndexOf(aux)]++;
                    }
                    else
                    {
                        listaFlujos.Add(aux);
                        numElementos.Add(1);
                    }
                }
                resultado += "<br /><div class='list-group'>";
                for (int i = 0; i < listaFlujos.Count; i++)
                {

                    resultado += @"<a href='#' onClick=""buscarTablaFlujos('" + listaFlujos[i] + @"')"" class='list-group-item'>
                                    <i class='fa fa-pencil-square-o'></i> " + listaFlujos[i] + @"
                                    <span class='pull-right text-muted small'><em>" + numElementos[i] + @"</em>
                                    </span>
                                </a>";
                }
                resultado += "</div>";


                return resultado;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getListaDeFlujosUsuario", "WorkFlow", idUsuario, e.ToString(), e.InnerException + "");
                return "-1";
            }
        }

        public static string getListaTotalDeFlujos()
        {
            string resultado = "";
            int cont = 0;
            try
            {

                DataTable flujos = DatabaseConnection.executeNonQueryDT("SELECT idFlujo, Descripcion, Activo, QR FROM Flujo", CommandType.Text, ConnectionString.WorkFlow);
                resultado += " <div class='panel-group' id='accordion'>";
                string color = "default";
                if(flujos.Rows.Count> 0){

                    foreach (DataRow dr in flujos.Rows)
                    {
                        if (cont == 0)
                        {
                            color = "info";
                            cont++;
                        }
                        else
                        {
                            color = "success";
                            cont = 0;
                        }
                        resultado += @"
                         <div class='panel panel-" + color + @"'>
                                    <div class='panel-heading'>
                                        <h4 class='panel-title'>
                                            <a data-toggle='collapse' data-parent='#accordion' href='#collapse" + dr["idFlujo"] + @"' class='collapsed'>" + dr["Descripcion"] + @"</a>
                                        </h4>
                                    </div>
                                    <div id='collapse" + dr["idFlujo"] + @"' class='panel-collapse collapse' style='height: 0px;'>
                                        <div class='panel-body form-inline'>
                                            
                                            <div class='col-md-3'>
                                            ";

                        if (dr["Activo"].Equals("S"))
                        {
                            resultado += "<input type='checkbox' checked><label> Activo </label><br />";
                        }
                        else
                        {
                            resultado += "<input type='checkbox'><label> Activo </label><br />";
                        }

                        if (Convert.ToBoolean(dr["QR"]))
                        {
                            resultado += "<input type='checkbox' checked><label> QR </label>";
                        }
                        else
                        {
                            resultado += "<input type='checkbox'><label> QR </label>";
                        }

                        resultado+= @"
                                        </div>
                                        <div class='col-md-9'>
                                            <button class='btn btn-default btn-lg'>Guardar</button>
                                            <button class='btn btn-default btn-lg'>Editar campos</button>
                                            <button class='btn btn-default btn-lg'>Editar pasos</button>
                                        </div>
                                    </div>
                                </div>
                            </div>";

                        //resultado += @"<a href='#' onClick=""mostrarEdicionFlujo('" + dr["idFlujo"].ToString() + @"')"" class='list-group-item'>
                        //            <i class='fa fa-pencil-square-o'></i> " + dr["Descripcion"].ToString() + "</a>";

                    }
                    resultado += "</div>";

                }
                return resultado;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getListaTotalDeFlujos", "WorkFlow", -1, e.ToString(), e.InnerException + "");
                return "-1";
            }
        }

        public static string subirFicheroAdjunto(string docOriginal, int idDocAdjunto, Byte[] kk)
        {
            String pathString = "";
            try
            {
                String idDocAux = idDocAdjunto + "";
                String path = "00000000".Substring(idDocAux.Length) + idDocAux;
                String[] aux = docOriginal.Split('.');
                String formato = aux[aux.Length - 1];

                pathString = WebConfigurationManager.AppSettings["ruta"] + "Adjuntos/" + path.Substring(0, 2) + "/" + path.Substring(2, 2) + "/" +
                    path.Substring(4, 2) + "/";

                System.IO.Directory.CreateDirectory(pathString);
                File.WriteAllBytes(pathString + "/" + path.Substring(6, 2) + "." + formato, kk);

                if (File.Exists(pathString + "/" + path.Substring(6, 2) + "." + formato))
                {
                    return "Documento subido.";
                }
                else
                {
                    return "Documento no subido.";
                }
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("subirFicheroAdjunto", "WorkFlow", -1, docOriginal + " . " + idDocAdjunto + " . " + e.ToString(), e.InnerException + "");
                return "Error subiendo fichero. Error: " + e.ToString();
            }
        }

        public static int insertarEnBBDD(string idFlujo, string idFicha, string docOriginal, int idUsuario)
        {
            String query = "";
            int res = -1;
            try
            {
                // Miramos si ese flujo ya tiene un documento principal

                query = "SELECT TOP 1 idDocumento FROM Documentos WHERE idFlujo = '" + idFlujo + "' AND idFicha = '" + idFicha + "'";
                DataTable dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                if (dt.Rows.Count > 0)
                {
                    res = Convert.ToInt32(dt.Rows[0]["idDocumento"]);
                }
                else
                {
                    query = "INSERT INTO Documentos (idFlujo, idFicha, FechaCreacion, idUsuario, docOriginal) VALUES ('" +
                        idFlujo + "', '" + idFicha + "', GETDATE(),'" + idUsuario + "', '" + docOriginal + "'); select SCOPE_IDENTITY()  ;";

                    res = Convert.ToInt32(DatabaseConnection.executeScalar(query,
                        CommandType.Text, ConnectionString.WorkFlow));

                }
                
                try{
                    int idEmpresa = BasicoFlujo.getIdEmpresa(idUsuario);
                    BasicoFlujo.insertarDocumentoUsuario(idUsuario, idUsuario, "", res);
                }catch(Exception ee){
                    Trazas t = new Trazas("insertarEnBBDD", "insertarDocumentoUsuario", idUsuario, docOriginal + " . " + idFlujo + " . " + idFicha + " . " + ee.ToString(), ee.InnerException + "");
                }

                return res;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("insertarEnBBDD", "WorkFlow", idUsuario, docOriginal + " . " + idFlujo + " . " + idFicha + " . " + e.ToString(), e.InnerException + "");
                return res;
            }
        }

        public static int insertarAdjuntoEnBBDD(string idDocumento, int idUsuario, string docOriginal)
        {
            String query = "";
            int res = -1;
            try
            {
                query = "INSERT INTO DocumentosAdjuntos (idDocumento, FechaCreacion, idUsuario, DocOriginal) VALUES ('" +
                    idDocumento + "', GETDATE(),'" + idUsuario + "', '" + docOriginal + "'); select SCOPE_IDENTITY()  ;";

                res = Convert.ToInt32(DatabaseConnection.executeScalar(query,
                    CommandType.Text, ConnectionString.WorkFlow));

                return res;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("insertarCamposFlujoCero", "WorkFlow", -1, idDocumento + "." + idUsuario + "." + docOriginal + "."
                    + e.ToString(), e.InnerException + "");
                return res;
            }
        }

        public static string getNombreYApellidosUsuario(string usuarioEmisor)
        {
            try
            {
                string query = "SELECT TOP 1 Nombre, Apellidos FROM Usuarios WHERE idUsuario = '" + usuarioEmisor + "'";
                DataTable dtUsuario = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                string nombre = dtUsuario.Rows[0]["Nombre"].ToString();
                string apellidos = "";

                if (!Convert.IsDBNull(dtUsuario.Rows[0]["Apellidos"]))
                {
                    apellidos = dtUsuario.Rows[0]["Apellidos"].ToString();
                }

                return nombre + " " + apellidos;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getNombreYApellidosUsuario", "WorkFlow", -1, usuarioEmisor + " . " + e.ToString(), e.InnerException + "");
                return "";
            }
        }

        public static string getMailUsuario(int idUsuario)
        {
            string mail = "";
            try
            {
                string query = "SELECT email FROM Usuarios WHERE idUsuario = '" + idUsuario + "'";
                mail = DatabaseConnection.executeScalar(query, CommandType.Text, ConnectionString.WorkFlow).ToString();
                return mail;

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getMailUsuario", "WorkFlow", -1, idUsuario + "." +
                    e.ToString(), e.InnerException + "");
                return "-1";
            }
        }

        public static int getIdEmpresa(int idUsuario)
        {
            try
            {
                string query = "SELECT TOP 1 idEmpresa FROM UsuarioEmpresa WHERE idUsuario = '" + idUsuario + "'";
                int idEmpresa = Convert.ToInt32(DatabaseConnection.executeScalar(query, CommandType.Text, ConnectionString.WorkFlow));
                return idEmpresa;
            }
            catch (Exception e)
            {

                Trazas t = new Trazas("getIdEmpresa", "WorkFlow", -1, idUsuario + "." +
                    e.ToString(), e.InnerException + "");
                return -1;
            }
        }

        public static string getNombreTabla(string idFlujo)
        {
            return "f" + "00000000".Substring(idFlujo.Length) + idFlujo;
        }

        public static bool flujoTieneEseCampo(string idFlujo, string nombreCampo)
        {
            try
            {
                string query = @"IF EXISTS( SELECT * FROM sys.columns
                                    WHERE Name = N'" + nombreCampo + "' AND OBJECT_ID = OBJECT_ID(N'" + getNombreTabla(idFlujo) + @"'))
                                    BEGIN 
	                                    select 1
	                                    END ";

                int resp = Convert.ToInt32(DatabaseConnection.executeScalar(query, CommandType.Text, ConnectionString.WorkFlow));
                if (resp == 1) return true;
                else return false;
            }
            catch (Exception e)
            {

                Trazas t = new Trazas("getIdEmpresa", "WorkFlow", -1, idFlujo + "." + nombreCampo + "." +
                    e.ToString(), e.InnerException + "");
                return false;
            }
        }

        public static bool getCoincideNombreConIdUsuario(string receptorHdd, string receptor)
        {
            try
            {
                string query = "SELECT TOP 1 idUsuario FROM Usuarios WHERE idUsuario = '" + receptorHdd + "' AND Nombre = '" + receptor + "'";
                DataTable dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                if (dt.Rows.Count > 0) return true;
                return false;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getCoincideNombreConIdUsuario", "WorkFlow", -1, receptorHdd + "." + receptor + "." +
                    e.ToString(), e.InnerException + "");
                return false;
            }
        }

        public static string getRutaDoc(string idDoc)
        {

            string path = "00000000".Substring(idDoc.Length) + idDoc;
            if (Convert.ToInt32(idDoc) < 10) idDoc = "0" + idDoc;

            string pathString = WebConfigurationManager.AppSettings["rutaVolumenes"] + path.Substring(0, 2) + "/" + path.Substring(2, 2) + "/" +
                    path.Substring(4, 2) + "/" + idDoc.Substring(idDoc.Length - 2, 2) + ".pdf";

            return pathString;
        }

        public static string getRutaDocAdjunto(string idDoc, string formato)
        {

            string path = "00000000".Substring(idDoc.Length) + idDoc;

            if (Convert.ToInt32(idDoc) < 10) idDoc = "0" + idDoc;

            string pathString = WebConfigurationManager.AppSettings["ruta"] + "Adjuntos/" + path.Substring(0, 2) + "/" + path.Substring(2, 2) + "/" +
                    path.Substring(4, 2) + "/" + idDoc.Substring(idDoc.Length - 2, 2) + "." + formato;

            return pathString;
        }

        public static Image GetImageFromUrl(string url)
        {
            using (var webClient = new WebClient())
            {
                return ByteArrayToImage(webClient.DownloadData(url));
            }
        }

        public static byte[] GetByteArrayFromUrl(string url)
        {
            try
            {
                using (var webClient = new WebClient())
                {
                    return webClient.DownloadData(url);
                }
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("GetByteArrayFromUrl", "WorkFlow", -1, url + " . " + e.ToString(), e.InnerException + "");
                return null;
            }
        }

        public static Image ByteArrayToImage(byte[] fileBytes)
        {
            using (var stream = new MemoryStream(fileBytes))
            {
                return Image.FromStream(stream);
            }
        }

        public static bool flujoTerminado(string idFlujo, string idFicha)
        {
            try
            {
                string query = "SELECT TOP 1 estado FROM " + getNombreTabla(idFlujo) + " WHERE idFicha = '" + idFicha + "'";
                char estado = Convert.ToChar(DatabaseConnection.executeScalar(query, CommandType.Text, ConnectionString.WorkFlow));

                if (estado == 'F') return true;
                else return false;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("flujoTerminado", "WorkFlow", -1, idFlujo + "." + idFicha + "." +
                    e.ToString(), e.InnerException + "");
                return false;
            }
        }

        public static bool estaObteniendoUbicaciones(string idFlujo, string idFicha)
        {
            try
            {
                // Quiero saber si el paso anterior tenía el seguimiento GPS
                // Para ello miro si el último paso aprobado tiene seguimiento

                string query = "SELECT TOP 1 SeguimientoGPS FROM Estados WHERE idFlujo = '" + idFlujo +
                    "' AND idFicha = '" + idFicha + "' AND FechaPasoFin is NOT NULL AND NumPaso < 1000 ORDER BY NumPaso DESC";

                DataTable dtAtributos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                if (dtAtributos.Rows.Count > 0)
                {
                    if (dtAtributos.Rows[0]["SeguimientoGPS"].ToString() == "True")
                    {
                        return true;
                    }
                }
                return false;

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("estaObteniendoUbicaciones", "WorkFlow", -1, idFlujo + "." + idFicha + "." +
                    e.ToString(), e.InnerException + "");
                return false;
            }
        }

        public static int generarSiguientePaso(string idFlujo, string idFicha, int siguientePaso, string tokenUsuario)
        {
            String query = "";
            int idUsuario = -1;
            Notificacion n = new Notificacion();
            try
            {

                idUsuario = Convert.ToInt32(tokenUsuario.Split('I')[0]);
                    query = "SELECT idEstado, idUsuarioAFirmar, TipoUsuario, NumPaso, idCondicion, idAccion FROM Estados WHERE idFlujo = '" + idFlujo + "' AND idFicha = '" + idFicha +
                        "' AND FechaPasoInicio IS NULL AND NumPaso < 1000 AND NumPaso = " + siguientePaso + " ORDER BY NumPaso ASC";
                    DataTable dtEstados = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                    if (dtEstados.Rows.Count > 0)
                    {

                        int numPaso = siguientePaso;

                        int numPasoAux = numPaso;
                        int cont = 0;
                        bool existenPasosPendientes = BasicoFlujo.getExistenPasosPendientes(idFlujo, idFicha);
                        while ((numPaso == numPasoAux) && (cont < dtEstados.Rows.Count))
                        {
                            int idEstado = Convert.ToInt32(dtEstados.Rows[cont]["idEstado"]);

                            int idCondicion = Convert.ToInt32(dtEstados.Rows[cont]["idCondicion"]);
                            int idAccion = Convert.ToInt32(dtEstados.Rows[cont]["idAccion"]);
                            if (Convert.ToInt32(idCondicion) == -1 && Convert.ToInt32(idAccion) == -1)
                            {
                                // Es un paso
                                // Insertamos este nuevo paso 
                                query = "UPDATE Estados SET FechaPasoInicio = GETDATE() WHERE idEstado = '" + idEstado + "'";
                                int res = Convert.ToInt32(DatabaseConnection.executeNonQueryInt(query, CommandType.Text,
                                    ConnectionString.WorkFlow));

                                // Comprobamos si ese usuario tiene la cuenta activa, de no ser así le enviamos un mail notificándole que tiene un documento pendiente de firmar
                                int idUsuarioAFirmar = Convert.ToInt32(dtEstados.Rows[cont]["idUsuarioAFirmar"]);

                                if (idUsuarioAFirmar > 0 && !Convert.IsDBNull(dtEstados.Rows[cont]["TipoUsuario"]) && Convert.ToChar(dtEstados.Rows[cont]["TipoUsuario"]) != ' ' )
                                {
                                    char tipoUsuario = Convert.ToChar(dtEstados.Rows[cont]["TipoUsuario"]);

                                    if (tipoUsuario == 'U')
                                    {
                                        // Comprobamos que tenga la cuenta activa, sino le enviamos mail
                                        query = "SELECT TOP 1 idUsuario, email FROM Usuarios WHERE idUsuario = '" + idUsuarioAFirmar + "' AND Activo = 'N'";
                                        DataTable dtUsuarios = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                                        if (dtUsuarios.Rows.Count > 0)
                                        {
                                            string strRes = BasicoFlujo.enviarInvitacion(idUsuario, dtUsuarios.Rows[cont]["email"].ToString(), idUsuarioAFirmar, Convert.ToInt32(idFlujo), Convert.ToInt32(idFicha));
                                        }
                                    }
                                }

                                // Miramos si requiere firma biométrica o certificada
                                // Si es así añadimos una carátula
                                query = "SELECT TOP 1 firmaCertificada, Biometria FROM estados WHERE idEstado = " + idEstado + " AND (NecesitaFirma = 'true' OR Biometria = 'true')";
                                DataTable dtFirma = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                                if (dtFirma.Rows.Count > 0)
                                {
                                    // Requiere firma, miramos si el documento está vacío
                                    query = "SELECT TOP 1 docOriginal FROM Documentos WHERE idFlujo = " + idFlujo + " AND idFicha = " + idFicha;
                                    string docOriginal = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow);
                                    if (string.IsNullOrEmpty(docOriginal))
                                    {
                                        query = "SELECT TOP 1 idDocumento FROM Documentos WHERE idFlujo = " + idFlujo + " AND idFicha = " + idFicha;
                                        string idDoc = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow);



                                        int resp = 1;
                                        if (string.IsNullOrEmpty(idDoc) || idDoc == "-1")
                                        {
                                            idDoc = BasicoFlujo.insertarEnBBDD(idFlujo, idFicha, "caratula.pdf", idUsuario) + "";
                                        }
                                        if(resp == 1){
                      
                                        }
                                    }

                                }


                            }
                            else
                            {

                                if (idCondicion > -1)
                                {
                                    // Es una condición
                                    
                                    // Lo pongo igual a true así si no hubiera condición también pasaría al siguiente paso
                                    bool cumpleCondicion = false;
                                    
                                    query = "SELECT TOP 1 Campo, Operador, Valor FROM Condiciones WHERE idCondicion = " + idCondicion;
                                    DataTable dtCondicion = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                                    if (dtCondicion.Rows.Count > 0)
                                    {
                                        string nombreCampoCond = dtCondicion.Rows[0]["Campo"].ToString();
                                        int OperadorCond = Convert.ToInt32(dtCondicion.Rows[0]["Operador"]);
                                        string valorCond = dtCondicion.Rows[0]["Valor"].ToString();
                                        string valorActualCampo = BasicoFlujo.getValueCampo(nombreCampoCond, idFlujo, idFicha);

                                        /*
                                         * 
                                         * <option value="1"> > </option>
                                         * <option value="2"> < </option>
                                         * <option value="3"> >= </option>
                                         * <option value="4"> <= </option>
                                         * <option value="5"> == </option>
                                         * 
                                         * */

                                        try{
                                            switch (OperadorCond)
                                            {
                                                // Pongo try y catch para que si son otros tipos no cumpla la condición pero tampoco salga

                                                case 1:
                                                    if (Convert.ToInt32(valorActualCampo) > Convert.ToInt32(valorCond)) cumpleCondicion = true;
                                                    break;
                                                case 2:
                                                    if (Convert.ToInt32(valorActualCampo) < Convert.ToInt32(valorCond)) cumpleCondicion = true;
                                                    break;
                                                case 3:
                                                    if (Convert.ToInt32(valorActualCampo) >= Convert.ToInt32(valorCond)) cumpleCondicion = true;
                                                    break;
                                                case 4:
                                                    if (Convert.ToInt32(valorActualCampo) <= Convert.ToInt32(valorCond)) cumpleCondicion = true;
                                                    break;
                                                case 5:
                                                    if ((valorActualCampo.Trim()) == valorCond.Trim()) cumpleCondicion = true;
                                                    break;

                                            }
                                        }
                                        catch { };

                                        // Marcamos la condición como finalizada
                                        char cAceptado;
                                        if(cumpleCondicion) cAceptado = 'S';
                                        else cAceptado = 'N';

                                        query = "UPDATE Estados SET FechaPasoInicio = GETDATE(), FechaPasoFin = GETDATE(), Aceptado = '" + cAceptado + "' WHERE idEstado = " + idEstado;
                                        int resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);

                                        List<int> pasosSiguientes = new List<int>();

                                        if (cumpleCondicion)
                                        {
                                            // Cogemos los pasos siguientes
                                            // pasosSiguientes = BasicoFlujo.getSiguientePaso(idFlujo, idFicha, numPaso, true);

                                            //workFlow.panelUsuario.handlerUsuario p = new workFlow.panelUsuario.handlerUsuario();
                                            //string tokenUsuario = Utilities.getParam(HttpContext.Current, "token");
                                            //string s = p.aprobarPaso(idEstado + "", HttpContext.Current, tokenUsuario, idFicha, "");

                                            pasosSiguientes = BasicoFlujo.getSiguientePaso(idFlujo, idFicha, numPaso, true);
                                        }
                                        else
                                        {
                                            // Cogemos los pasos siguientes
                                           
                                            pasosSiguientes = BasicoFlujo.getSiguientePaso(idFlujo, idFicha, numPaso, false);

                                        }

                                        // Iniciamos esos pasos
                                        for (int i = 0; i < pasosSiguientes.Count; i++)
                                        {
                                            bool existenPasosPrevios = BasicoFlujo.getExistenPasosPrevios(idFlujo, idFicha, pasosSiguientes[i]);

                                            if (pasosSiguientes[i] == -1)
                                            {
                                                if (existenPasosPendientes)
                                                {
                                                    n.setNotificacionFlujoAprobado(Convert.ToInt32(idFlujo), Convert.ToInt32(idFicha), Convert.ToInt32(idUsuario));
                                                }
                                                else
                                                {
                                                    int respuesta = BasicoFlujo.terminarFlujo(idFlujo, idFicha);

                                                    n.setNotificacionFlujoTerminado(Convert.ToInt32(idFlujo), Convert.ToInt32(idFicha), Convert.ToInt32(idUsuario));

                                                    workFlow.classes.Utilities.addEvento("Flujo terminado", "Flujo terminado. ", idUsuario + "", null, idEstado + "");
                                                }
                                            }
                                            else
                                            {
                                                // Miramos si antes de pasar al siguiente paso necesitamos que se aprueben otros
                                                if (pasosSiguientes[i] == Convert.ToInt32(numPaso) || existenPasosPrevios)
                                                {
                                                    n.setNotificacionFlujoAprobado(Convert.ToInt32(idFlujo), Convert.ToInt32(idFicha), Convert.ToInt32(idUsuario));
                                                }
                                                else
                                                {
                                                    int respuesta = BasicoFlujo.generarSiguientePaso(idFlujo, idFicha, pasosSiguientes[i], tokenUsuario);
                                                    n.setNotificacionFlujoAprobado(Convert.ToInt32(idFlujo), Convert.ToInt32(idFicha), Convert.ToInt32(idUsuario));
                  
                                                }
                                            }
                                        }

                                    }



                                }
                                else
                                {
                                    if (idAccion > 0)
                                    {

                                        // Actualizamos como iniciado
                                        query = "UPDATE Estados set FechaPasoInicio = GETDATE() WHERE idEstado = " + idEstado;
                                        int resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);

                                        Acciones acc = new Acciones(idEstado, idUsuario, tokenUsuario);
                                        List<int> pasosSiguientes = new List<int>();
                                        pasosSiguientes = BasicoFlujo.getSiguientePaso(idFlujo, idFicha, numPaso, true);

                                        // Actualizamos como finalizado
                                        query = "UPDATE Estados set FechaPasoFin = GETDATE(), Aceptado = 'S', TipoUsuario = 'U', idUsuario = '" + 
                                            idUsuario + "'  WHERE idEstado = " + idEstado;
                                        resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);

                                        // Iniciamos esos pasos
                                        for (int i = 0; i < pasosSiguientes.Count; i++)
                                        {

                                            bool existenPasosPrevios = BasicoFlujo.getExistenPasosPrevios(idFlujo, idFicha, pasosSiguientes[i]);

                                            if (pasosSiguientes[i] == -1)
                                            {
                                                if (existenPasosPendientes)
                                                {
                                                    n.setNotificacionFlujoAprobado(Convert.ToInt32(idFlujo), Convert.ToInt32(idFicha), Convert.ToInt32(idUsuario));
                                                }
                                                else
                                                {
                                                    int respuesta = BasicoFlujo.terminarFlujo(idFlujo, idFicha);

                                                    n.setNotificacionFlujoTerminado(Convert.ToInt32(idFlujo), Convert.ToInt32(idFicha), Convert.ToInt32(idUsuario));

                                                    workFlow.classes.Utilities.addEvento("Flujo terminado", "Flujo terminado. ", idUsuario + "", null, idEstado + "");
                                                }
                                            }
                                            else
                                            {
                                                // Miramos si antes de pasar al siguiente paso necesitamos que se aprueben otros
                                                if (pasosSiguientes[i] == Convert.ToInt32(numPaso) || existenPasosPrevios)
                                                {
                                                    n.setNotificacionFlujoAprobado(Convert.ToInt32(idFlujo), Convert.ToInt32(idFicha), Convert.ToInt32(idUsuario));
                                                }
                                                else
                                                {
                                                    int respuesta = BasicoFlujo.generarSiguientePaso(idFlujo, idFicha, pasosSiguientes[i], tokenUsuario);
                                                    n.setNotificacionFlujoAprobado(Convert.ToInt32(idFlujo), Convert.ToInt32(idFicha), Convert.ToInt32(idUsuario));
                                                }
                                            }
                                        }
                                    }

                                }


                            }


                            cont++;
                            if (cont < dtEstados.Rows.Count)
                            {
                                numPasoAux = Convert.ToInt32(dtEstados.Rows[cont]["NumPaso"]);
                            }
                            else
                            {
                                numPasoAux = -1;
                            }

                        }

                    }
                    else
                    {
                        // Miramos si ya ha existido, de ser así lo volvemos a crear 
                        // Sino devolvemos menos uno

                        query = "SELECT idEstado FROM Estados WHERE idFlujo = " + idFlujo + " AND idFicha = " + idFicha + " AND NumPaso = " + siguientePaso;
                        DataTable dtEstado = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                        if (dtEstado.Rows.Count > 0)
                        {
                            query = "SELECT TOP 1 idPaso, NumPaso, Descripcion, Usuario, TipoUsuario, NecesitaFirma, NumeroFirmas, Biometria, " +
                                    "InformacionAdicional, EnvioLibre, SeguimientoGPS, EnviarNotificacion, ExportarCSV, pasoOK, pasoNOK, pasoPRE, " +
                                    "ExportarXML, idCondicion, idAccion FROM " +
                                    "Pasos WHERE idFlujo = '" + idFlujo + "' AND NumPaso = " + siguientePaso + "ORDER BY NumPaso ASC";
                            DataTable dtSiguientePaso = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                            query = "INSERT INTO Estados (idFlujo, idFicha, NumPaso, Activo, TipoUsuario, " +
                                "idUsuarioAFirmar, Bloqueado, DescripcionPaso, InformacionAdicional, firmaCertificada, " +
                                "Biometria, NumeroFirmas, EnvioLibre, idPaso, SeguimientoGPS, EnviarNotificacion, " +
                                "ExportarCSV, NecesitaFirma, pasoOK, pasoNOK, pasoPRE, ExportarXML, idCondicion, idAccion) VALUES " +
                            "('" + idFlujo + "', '" + idFicha + "', '" + siguientePaso + "', 1, '" +
                            dtSiguientePaso.Rows[0]["TipoUsuario"] + "', '" + dtSiguientePaso.Rows[0]["Usuario"] + "',0,'" + 
                            dtSiguientePaso.Rows[0]["Descripcion"] + "','" + dtSiguientePaso.Rows[0]["InformacionAdicional"] + "','" + 
                            dtSiguientePaso.Rows[0]["NecesitaFirma"] + "','" + dtSiguientePaso.Rows[0]["Biometria"] + "','" + 
                            dtSiguientePaso.Rows[0]["NumeroFirmas"] + "','" + dtSiguientePaso.Rows[0]["EnvioLibre"] + "','" + 
                            dtSiguientePaso.Rows[0]["idPaso"] + "', '" + dtSiguientePaso.Rows[0]["SeguimientoGPS"] + "','" + 
                            dtSiguientePaso.Rows[0]["EnviarNotificacion"] + "', '" + dtSiguientePaso.Rows[0]["ExportarCSV"] + "','" + 
                            dtSiguientePaso.Rows[0]["NecesitaFirma"] + "','" + dtSiguientePaso.Rows[0]["pasoOK"] + "','" + 
                            dtSiguientePaso.Rows[0]["pasoNOK"] + "','" + dtSiguientePaso.Rows[0]["pasoPRE"] + "','" + 
                            dtSiguientePaso.Rows[0]["ExportarXML"] + "','" + dtSiguientePaso.Rows[0]["idCondicion"] + "','" + 
                            dtSiguientePaso.Rows[0]["idAccion"] + "')";

                            int resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);
                            if (resp == 1)
                            {
                                resp = generarSiguientePaso(idFlujo, idFicha, siguientePaso, tokenUsuario);
                                //query = "UPDATE Estados SET FechaPasoInicio = GETDATE() WHERE idFlujo = " + idFlujo + " AND idFicha = " + idFicha + " AND NumPaso = " + siguientePaso + " AND FechaPasoInicio IS NULL AND FechaPasoFin IS NULL";
                                //resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);
                            }
                            return resp;
                        }
                        else
                        {
                            return -1;

                        }
                    }
                
                return 1;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("generarSiguientePaso", "WorkFlow", idUsuario, idFlujo + "." + idFicha + "." +
                    e.ToString(), e.InnerException + "");
                return -1;
            }
        }

        public static string getValueCampo(string nombre, string idFlujo, string idFicha)
        {
            string resultado = "";
            try
            {
                string query = "SELECT top 1 " + nombre + " FROM " + BasicoFlujo.getNombreTabla(idFlujo) +
                    " WHERE idFicha= '" + idFicha + "' ";
                resultado = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow).ToString();
                return resultado;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getValueCampo", "WorkFlow", -1, nombre + " . " + idFlujo + " . " + idFicha +
                  e.ToString(), e.InnerException + "");
                return resultado;
            }

        }


        public static string subirFichero(string docOriginal, int idDoc, Byte[] kk)
        {
            string pathString = "";
            try
            {
                string idDocAux = idDoc + "";
                string path = "00000000".Substring(idDocAux.Length) + idDocAux;
                string[] aux = docOriginal.Split('.');
                string formato = aux[aux.Length - 1].ToUpper();

                // Obtenemos el idusuario del que ha subido el fichero
                int idUsuario = Convert.ToInt32(DatabaseConnection.executeScalar("SELECT TOP 1 idUsuario FROM Documentos WHERE idDocumento = '" + idDoc + "'", CommandType.Text, ConnectionString.WorkFlow));

                pathString = WebConfigurationManager.AppSettings["rutaVolumenes"] + path.Substring(0, 2) + "/" + path.Substring(2, 2) + "/" +
                    path.Substring(4, 2) + "/";

                System.IO.Directory.CreateDirectory(pathString);

                File.WriteAllBytes(pathString + "/" + path.Substring(6, 2) + "." + formato, kk);


                if (File.Exists(pathString + "/" + path.Substring(6, 2) + "." + formato))
                {
                    workFlow.classes.Utilities.addEvento("Subir fichero", "Subir fichero, tamaño fichero:  " + kk.Length, idUsuario + "", idDoc + "");

                    return "Documento subido.";
                }
                else
                {
                    return "Documento no subido.";
                }
            }
            catch (Exception e)
            {
                return "Error subiendo fichero. Error: " + e.ToString();
            }
        }


        public static List<int> getSiguientePaso(string idFlujo, string idFicha, int numPasoActual, bool aprobado)
        {
            string query = string.Empty;
            int pasoOK = -1;
            //bool cumpleCondiciones = true;
            List<int> siguientesPasos = new List<int>();
            
            try
            {
                int versionFlujo = BasicoFlujo.getVersionFlujo(idFlujo);
                if (versionFlujo == 1)
                {

                    query = "SELECT TOP 1 NumPaso FROM Estados WHERE (idFlujo = '" + idFlujo + "') " +
                        "AND (idFicha = '" + idFicha + "') AND (NumPaso <= '999') AND FechaPasoFin is null ORDER BY NumPaso ASC";
               

                        // La fila existe
                        // Si devuelve vacío devolvemos menos uno, sino el siguiente paso
                    string obj = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow);
                        if (string.IsNullOrEmpty(obj))
                        {
                            siguientesPasos.Add(-1);
                            return siguientesPasos;
                        }

                        pasoOK = Convert.ToInt32(obj);
                        siguientesPasos.Add(Convert.ToInt32(obj));
                        return siguientesPasos;

                    
                }else{
                    // Es nueva versión
                    // Primero miramos si tiene pendiente algún paso del inicio

                    query = "SELECT NumPaso FROM Estados WHERE idFlujo = " + idFlujo + " AND idFicha = " + idFicha + " AND pasoPRE like '%startpoint%' AND FechaPasoFin IS NULL and FechaPasoInicio IS NULL";
                    DataTable dtEstados = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                    if (dtEstados.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtEstados.Rows.Count; i++)
                        {
                            siguientesPasos.Add(Convert.ToInt32(dtEstados.Rows[i]["NumPaso"]));
                        }
                    }


                    // Ahora añadimos los siguientes pasos dependiendo de si se ha aprobado o rechazado
                    query = "SELECT TOP 1 pasoOK, pasoNOK FROM Estados WHERE idFlujo = " + idFlujo + " AND idFicha = " + idFicha + " AND NumPaso = " + numPasoActual;
                    DataTable dtPasoSig = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                    if (dtPasoSig.Rows.Count > 0)
                    {
                        // Contiene filas
                        object pasoSig = null;
                        if (aprobado)
                        {
                            pasoSig = dtPasoSig.Rows[0]["pasoOK"];
                        }
                        else
                        {
                            pasoSig = dtPasoSig.Rows[0]["pasoNOK"];
                        }

                        if (pasoSig != null && !Convert.IsDBNull(pasoSig) && !string.IsNullOrEmpty(pasoSig.ToString()))
                        {
                            string[] pasosSig = pasoSig.ToString().Split(',');

                            for (int i = 0; i < pasosSig.Length; i++)
                            {
                                if (!string.IsNullOrEmpty(pasosSig[i]))
                                {
                                    siguientesPasos.Add(Convert.ToInt32(pasosSig[i]));
                                }
                            }
                        }
                    }
                   
                    
                    return siguientesPasos;


                }
           
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getSiguientePaso", "WorkFlow", -1, idFlujo + "." + idFicha + "." +
                    e.ToString(), e.InnerException + "");


                siguientesPasos.Add(-1);
                return siguientesPasos;

            }
        }

        public static int getVersionFlujo(string idFlujo)
        {
            string query = string.Empty;
            try
            {


                    query = "SELECT Version FROM Flujo WHERE idFlujo = " + idFlujo;
                    int version = Convert.ToInt32(DatabaseConnection.executeScalar(query, CommandType.Text, ConnectionString.WorkFlow));


                return version;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getVersionFlujo", "WorkFlow", -1, idFlujo + e.Message, e.ToString());
                return -1;
            }
        }


        public static string enviarInvitacion(int idUsuario, string email, int idUsuarioAFirmar, int idFlujo, int idFicha)
        {
            string query = string.Empty;
            try
            {
                // Registramos la invitación
                query = "INSERT INTO Invitaciones (idUsuarioEmisor, mailReceptor) VALUES ('" + idUsuario + "','" + 
                    email + "')";
                int resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);
                if (resp == 1)
                {
                    
                    string nombreEmisor = BasicoFlujo.getNombreYApellidosUsuario(idUsuario + "");
                    string body = getBodyInvitacion(email, nombreEmisor);
                    string asunto = getAsuntoInvitacion(email, nombreEmisor);


                    string dni = string.Empty;
                    if (idFlujo != -1 && idFicha != -1)
                    {
                        dni = BasicoFlujo.getValueCampo("DNI", idFlujo + "", idFicha + "");
                        if (string.IsNullOrEmpty(dni))
                        {
                            dni = "Zertifika";
                        }
                        else
                        {

                        }
                        body = getBodyFirma(email, nombreEmisor, dni);
                        //body = "nombre: " + email + "     clave: " + dni;
                    }

                   
                    resp = Utilities.enviarMail(idUsuario, email, asunto, body, true);

                    // El mail se ha enviado bien, creamos la fila del usuario
                    if (resp == 1)
                    {
                        if (idUsuarioAFirmar == -1)
                        {
                            

                            if (string.IsNullOrEmpty(dni))
                            {
                                // Creamos su fila
                                query = "INSERT INTO Usuarios (idTipoUsuario, Nombre, email, Activo) VALUES (2, '" + email + "', '" + email + "', 'S'); select SCOPE_IDENTITY();";
                                idUsuarioAFirmar = Convert.ToInt32(DatabaseConnection.executeScalar(query, CommandType.Text, ConnectionString.WorkFlow));
                            }
                            else
                            {
                                // Creamos su fila
                                // Si tenemos su dni ya montamos el usuario funcional, con la cuenta activa
                                //query = "INSERT INTO Usuarios (idTipoUsuario, Nombre, email, Activo, Password) VALUES (2, '" + email + "', '" + email + "', 'S', '" + dni + "'); select @@identity;";
                                query = "UPDATE Usuarios SET Nombre = '" + email + "', Email = '" + email + "', Password = '" + dni + "', Activo = 'S' WHERE idUsuario = " + idUsuarioAFirmar;
                                //idUsuarioAFirmar = Convert.ToInt32(DatabaseConnection.executeScalar(query, CommandType.Text, ConnectionString.WorkFlow));
                                resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);
                            }

                            

                            int idEmpresa = BasicoFlujo.getIdEmpresa(idUsuario);

                            // Falta crearlo para la empresa 
                            query = "INSERT INTO UsuarioEmpresa (idEmpresa, idUsuario, Administrador, Activo, FechaCreacion) VALUES " +
                                "('" + idEmpresa + "','" + idUsuarioAFirmar + "','False', 'True', GETDATE()) ";
                            resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);

                        }

                        // Actualizamos con su idUsuario
                        query = "UPDATE Invitaciones SET idUsuarioReceptor = '" + idUsuarioAFirmar + "' WHERE email = '" + email + "' ORDER BY FechaEmision DESC";
                        resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);



                        return "1#Invitación enviada correctamente.";
                    }
                    else
                    {
                        Trazas t = new Trazas("enviarInvitacion", "WorkFlow", idUsuario, "Error al crear la fila del usuario", query);
                    }


                }

                return "-1#Ha ocurrido un error enviando la invitación.";
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("enviarInvitacion", "WorkFlow", idUsuario, e.Message, e.ToString());
                return "-1#Ha ocurrido un error enviando la invitación.";
            }
        }

        private static string getBodyFirma(string email, string nombreEmisor, string dni)
        {
            string respuesta = string.Empty;
            string query = string.Empty;

            try
            {
                query = "SELECT TOP 1 token FROM Invitaciones WHERE mailReceptor = '" + email + "'";
                string token = DatabaseConnection.executeScalar(query, CommandType.Text, ConnectionString.WorkFlow).ToString();

                string _htmlheader = "<table width='98%' style='width: 98%;'>" +
                                       "<tr>" +
                                         "<td width='100%' style='width: 100%; padding: 30pt;'>" +
                                             "<table style='font-family: arial; font-size: 12px; width: 631px;'>" +
                                                 "<tr><td style='background: none repeat scroll 0% 0% #1f497d; padding-left:25px; height:32px; font-size:23px; font-family: Helvetica;'><p>" +
                                                    "<span style='color: white; letter-spacing: -0.35pt;'>ZFLOW<b><span style='color: white; font-size:9px;'></span></b></p>" +
                                                 "</tr>" +
                                                 "<tr>" +
                                                     "<td valign='top' style='border-width: medium 1pt 1pt; border-style: none solid solid; border-color: #385D8A; background: white; padding: 25px;' colspan='2'>";


                string _htmlSignature = "<div style='font-size:13px; padding: 10px 0 0 25px;'>";
                _htmlSignature += "<label>" + "El usuario " + nombreEmisor + " le ha enviado un documento para firmar. <br>";
                _htmlSignature += "Para firmar el documento instale el fichero adjunto de este mail y acceda con los siguientes credenciales: <br>";


                _htmlSignature += "</label> <b>Usuario:</b> " + email;
                _htmlSignature += "<b>Clave:</b> " + dni;

                _htmlSignature += "</div>";

                _htmlSignature += "<div style='border-width: medium medium 1pt; border-style: none none solid; border-color: rgb(204, 204, 204); padding: 0cm;'><p><span style='font-size: 9pt;'>&nbsp;</span></p></div><p><span style='font-size: 9pt;'></span></p></div>";




                string _footer = "<div style='font-size:10px; color: gray;'>";
                _footer += "<label><b>" + "Navegadores recomendados: Firefox, Chrome, Safari e Internet Explorer a partir de la versión 8." + "</b></label><br />";

                string _htmlfooter = _footer +
                                              "</td>" +
                                            "</tr>" +
                                         "</table></td></tr></table>";



                return _htmlheader + _htmlSignature + _htmlfooter;


            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getBodyInvitacion", "WorkFlow", -1, e.ToString(), e.InnerException + "");
                return "-1";
            }
        }

        private static string getAsuntoInvitacion(string email, string nombreEmisor)
        {
            try
            {
                // Miramos si tiene documentos pendientes de firma 
                int idUsuarioReceptor = BasicoFlujo.getIdUsuarioPorMail(email);
                DataTable dt = DatabaseConnection.executeNonQueryDT("EXEC getDocumentosAFirmar2 " + idUsuarioReceptor, CommandType.Text, ConnectionString.WorkFlow);
                if (dt.Rows.Count > 0)
                {
                    return "Tienes " + dt.Rows.Count + " documentos pendientes!";
                }
                else
                {
                    return "Invitacion de " + nombreEmisor + " para ZFlow";
                }
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getAsuntoInvitacion", "WorkFlow", -1, email + "." + nombreEmisor + "." + e.Message, e.ToString());
                return "Invitacion de " + nombreEmisor + " para ZFlow";
            }
        }

        public static int getIdUsuarioPorMail(string email)
        {
            try
            {
                string query = "SELECT TOP 1 idUsuario FROM Usuarios WHERE email = '" + email + "'";
                int idUsuario = Convert.ToInt32(DatabaseConnection.executeScalar(query, CommandType.Text, ConnectionString.WorkFlow));
                return idUsuario;
                
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getIdUsuarioPorMail", "WorkFlow", -1, email + "." + e.Message, e.ToString());
                return -1;
            }
        }

        public static string getNombreDocumento(string idDoc)
        {
            string query = "SELECT TOP 1 docOriginal FROM Documentos WHERE idDocumento = '" + idDoc + "'";
            string idDocumento = DatabaseConnection.executeScalar(query, CommandType.Text, ConnectionString.WorkFlow).ToString();
            return idDocumento;
        }

        private static string getBodyInvitacion(string email, string nombreUsuarioEmisor)
        {
            string respuesta = string.Empty;
            string query = string.Empty;

            try
            {
                query = "SELECT TOP 1 token FROM Invitaciones WHERE mailReceptor = '" + email + "'";
                string token = DatabaseConnection.executeScalar(query, CommandType.Text, ConnectionString.WorkFlow).ToString();

                string _htmlheader = "<table width='98%' style='width: 98%;'>" +
                                       "<tr>" +
                                         "<td width='100%' style='width: 100%; padding: 30pt;'>" +
                                             "<table style='font-family: arial; font-size: 12px; width: 631px;'>" +
                                                 "<tr><td style='background: none repeat scroll 0% 0% #1f497d; padding-left:25px; height:32px; font-size:23px; font-family: Helvetica;'><p>" +
                                                    "<span style='color: white; letter-spacing: -0.35pt;'>ZFLOW<b><span style='color: white; font-size:9px;'></span></b></p>" +
                                                 "</tr>" +
                                                 "<tr>" +
                                                     "<td valign='top' style='border-width: medium 1pt 1pt; border-style: none solid solid; border-color: #385D8A; background: white; padding: 25px;' colspan='2'>";


                string _htmlSignature = "<div style='font-size:13px; padding: 10px 0 0 25px;'>";
                _htmlSignature += "<label>" + "El usuario " + nombreUsuarioEmisor + "le ha enviado una invitación a ZFlow. <br>";
                _htmlSignature += "Para registrar su cuenta pulse sobre el siguiente enlace:";


                _htmlSignature += "</label><a style='color:#005f98; text-decoration: none;' href='http://62.43.192.130:83/signesDemos/Registro/index.aspx?inv=" + token + "&m=" + email + "&guid=1'> Acceder </b></a>";

                _htmlSignature += "</div>";

                _htmlSignature += "<div style='border-width: medium medium 1pt; border-style: none none solid; border-color: rgb(204, 204, 204); padding: 0cm;'><p><span style='font-size: 9pt;'>&nbsp;</span></p></div><p><span style='font-size: 9pt;'></span></p></div>";




                string _footer = "<div style='font-size:10px; color: gray;'>";
                _footer += "<label><b>" + "Navegadores recomendados: Firefox, Chrome, Safari e Internet Explorer a partir de la versión 8." + "</b></label><br />";

                string _htmlfooter = _footer +
                                              "</td>" +
                                            "</tr>" +
                                         "</table></td></tr></table>";



                return _htmlheader + _htmlSignature + _htmlfooter;


            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getBodyInvitacion", "WorkFlow", -1, e.ToString(), e.InnerException + "");
                return "-1";
            }
        }

        public static string getIdDocumento(string idFlujo, string idFicha)
        {
            try
            {
                string query = "SELECT TOP 1 idDocumento FROM Documentos WHERE idFlujo = '" +
                    idFlujo + "' AND idFicha = '" + idFicha + "'";
                string idDocumento = DatabaseConnection.executeScalar(query, CommandType.Text, ConnectionString.WorkFlow).ToString();
                return idDocumento;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getIdDocumento", "WorkFlow", -1, idFlujo + " . " + idFicha + " . " + e.ToString(), e.InnerException + "");
                return "-1";
            }
        }

        public static string getNombreFlujo(string idFlujo)
        {
            string query = "SELECT TOP 1 Descripcion FROM Flujo WHERE idFlujo = '" + idFlujo + "'";
            string nombre = DatabaseConnection.executeScalar(query, CommandType.Text, ConnectionString.WorkFlow).ToString();
            return nombre;
        }

        public static bool esImagen(string nombre)
        {
            try
            {
                if ((nombre != null) && (!string.IsNullOrEmpty(nombre) && (nombre.Contains('.'))))
                {
                    string formato = nombre.Split('.')[1];
                    if (!string.IsNullOrEmpty(formato) && ((formato.ToLower() == "jpg") || (formato.ToLower() == "png") || (formato.ToLower() == "gif") ||
                        (formato.ToLower() == "bmp") || (formato.ToLower() == "jpeg")))
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("esImagen", "WorkFlow", -1, nombre + "." + e.ToString(), e.InnerException + "");
                return false;
            }
        }

        public static string getNombreTablaCampos(string idTipoCampo)
        {
            if (string.IsNullOrEmpty(idTipoCampo)) return "";
            return "c" + "00000000".Substring(idTipoCampo.Length) + idTipoCampo;
        }

        public static string getRutaJSONFlujo(string idFlujo)
        {
            string path = "00000000".Substring(idFlujo.Length) + idFlujo;

            if (Convert.ToInt32(idFlujo) < 10) idFlujo = "0" + idFlujo;

            string pathString = WebConfigurationManager.AppSettings["ruta"] + "JSONFlujos/" + path.Substring(0, 2) + "/" + path.Substring(2, 2) + "/" +
                    path.Substring(4, 2) + "/" + idFlujo.Substring(idFlujo.Length - 2, 2) + ".txt";

            return pathString;
        }

        public static string cargarMensajeria(string idFlujo, string idFicha)
        {
            string respuesta = "";
            try
            {
                string query = "select idMensaje, idEstado, FechaCreacion, usu.idUsuario, Importante, Mensaje, Nombre from Mensajes men " +
                               "INNER JOIN Usuarios usu ON usu.idUsuario = men.idUsuario WHERE idFlujo = '" + idFlujo + "' AND idFicha = '" + idFicha + "' ORDER BY men.FechaCreacion DESC";
                DataTable dtMensajes = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                if (dtMensajes.Rows.Count > 0)
                {

                    foreach (DataRow dr in dtMensajes.Rows)
                    {


                        respuesta += "<label>" + dr["Nombre"] + " </label> [" + dr["FechaCreacion"] + "]: <br /> ";
                        respuesta += dr["Mensaje"] + " <hr>";

                    }



                }
                else
                {
                    respuesta += "<label> No hay mensajes </label> <br /> ";
                    respuesta += " <hr>";
                }

                respuesta += "<div class='form-inline'>";
                respuesta += "<input type='text' class='form-control' id='txtMensaje' name='txtMensaje' />";
                respuesta += "<button class='btn btn-primary' onClick='enviarMensajeInterno(" + idFlujo + ", " + idFicha + ");'>Enviar</button>";

                respuesta += "</div>";

                return respuesta;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("cargarMensajeria", "infoFlujo", -1, idFlujo + " . " + idFicha + " . " + e.ToString(), e.Message);
                return "-1";
            }
        }


        public static bool getExistenPasosPrevios(string idFlujo, string idFicha, int numPaso)
        {
            string query = string.Empty;
            bool existenPasosPrevios = false;
            try
            {
                // Miramos si hay pasos que tienen que finalizar antes de generarlo
                query = "SELECT TOP 1 pasoPRE FROM Estados WHERE idFlujo = " + idFlujo + " AND idFicha = " + idFicha + " AND NumPaso = " + numPaso;
                string pasoPRE = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow);
                if (!string.IsNullOrEmpty(pasoPRE))
                {
                    // Elimino los pasos inicio y finales ya que aquí no nos influyen
                    pasoPRE = pasoPRE.Replace("startpoint", "").Replace("endpoint", "");
                    string[] pasosPRE = pasoPRE.Split(',');
                    for (int i = 0; i < pasosPRE.Length - 1; i++)
                    {
                        int numPasoPre = Convert.ToInt32(pasosPRE[i]);

                        query = "SELECT TOP 1 idEstado FROM Estados WHERE idFlujo = " + idFlujo + " AND idFicha = " + idFicha + " AND NumPaso = " + numPasoPre +
                            " AND FechaPasoFin IS NULL AND FechaPasoInicio IS NOT NULL";

                        //query = "SELECT TOP 1 idEstado FROM Estados WHERE idFlujo = " + idFlujo + " AND idFicha = " + idFicha + " AND NumPaso = " + numPasoPre +
                        //    " AND FechaPasoInicio IS NOT NULL AND FechaPasoFin IS NULL";
                        
                        
                        DataTable dtEstado = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);


                        if (dtEstado.Rows.Count > 0)
                        {
                            existenPasosPrevios = true;
                            // Ya hemos encontrado que hay alguno, podemos salir
                            return existenPasosPrevios;
                        }
                    }
                }
                return existenPasosPrevios;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getExistenPasosPrevios", "WorkFlow", -1, idFlujo + " . " + idFicha + "." + numPaso + "." + e.Message, e.ToString());
                return existenPasosPrevios;
            }
        }

        internal static bool getExistenPasosPendientes(string idFlujo, string idFicha)
        {
            string query = string.Empty;
            bool existenPasosPendientes = false;
            try
            {
                // Miramos si hay pasos pendientes 
                query = "SELECT TOP 1 idEstado FROM Estados WHERE idFlujo = " + idFlujo + " AND idFicha = " + idFicha + " AND FechaPasoInicio IS NOT NULL AND FechaPasoFin IS NULL";
                DataTable dtEstadosPendientes = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                if (dtEstadosPendientes.Rows.Count > 0)
                {
                    existenPasosPendientes = true;


                }
                return existenPasosPendientes;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getExistenPasosPendientes", "WorkFlow", -1, idFlujo + " . " + idFicha + "." + e.Message, e.ToString());
                return existenPasosPendientes;
            }
        }

        internal static int generarPasoRechazoResponsable(string idFlujo, string idFicha, string tokenUsuario)
        {
            int resp = -1;
            string query = string.Empty;
            try
            {
                int idResponsable = getIdResponsableFlujo(idFlujo);
                if (idResponsable > -1)
                {
                    query = "INSERT INTO Estados (idFlujo, idFicha, NumPaso, FechaPasoInicio, idUsuarioAFirmar, TipoUsuario) VALUES " +
                        "(" + idFlujo + "," + idFicha + ",1001, GETDATE(), " + idResponsable + ",'U')";

                    resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);
                }
                return resp;

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("generarPasoRechazoResponsable", "WorkFlow", -1, idFlujo + " . " + idFicha + "." + e.Message, e.ToString());
                return resp;
            }
        }

        private static int getIdResponsableFlujo(string idFlujo)
        {
            try{
                int resp = Convert.ToInt32(DatabaseConnection.executeScalarString(
                    "SELECT TOP 1 idResponsable FROM Flujo WHERE idFlujo = " + 
                    idFlujo, CommandType.Text, ConnectionString.WorkFlow));

                return resp;

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getIdResponsableFlujo", "WorkFlow", -1, idFlujo + "." + e.Message, e.ToString());
                return -1;
            }
        }


        public static int terminarFlujo(string idFlujo, string idFicha)
        {
            String query = "";
            try
            {
                String nombreTabla = BasicoFlujo.getNombreTabla(idFlujo);
                query = "UPDATE " + nombreTabla + " set estado = 'F' WHERE idFicha = '" + idFicha + "'";
                int resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);

                string masTrazas = "";
                masTrazas += "respuesta de update = " + resp + Environment.NewLine;
                if (resp > 0)
                {
                    string nombreFlujo = BasicoFlujo.getNombreFlujo(idFlujo);
                    string idDocumento = BasicoFlujo.getIdDocumento(idFlujo, idFicha);
                    if (idDocumento != "-1")
                    {
                        string nombreDocumento = BasicoFlujo.getNombreDocumento(idDocumento);
                        //byte[] documento = descargaDoc("jose ramirez", "123456", idFlujo, idFicha, idDocumento);
                        // masTrazas += "nombreFlujo = " + nombreFlujo + Environment.NewLine;
                        // masTrazas += "idDocumento = " + idDocumento + Environment.NewLine;
                        // masTrazas += "nombreDocumento = " + nombreDocumento + Environment.NewLine;

                        string path = "00000000".Substring(idDocumento.Length) + idDocumento;

                        string pathString = WebConfigurationManager.AppSettings["rutaVolumenes"] + path.Substring(0, 2) + "/" + path.Substring(2, 2) + "/" +
                                path.Substring(4, 2) + "/" + idDocumento.Substring(idDocumento.Length - 2, 2) + "." + "pdf";

                        // masTrazas += "pathString = " + pathString + Environment.NewLine;
                        ////byte[] documento = File.ReadAllBytes(pathString);
                        // masTrazas += "llegamos a leer el documento" + Environment.NewLine;
                        ////wszfk.Service1 wf = new wszfk.Service1();
                        // int resp2 = Convert.ToInt32(
                        //string respppp = wf.crearExpedienteConFichero("josearamz@gmail.com", "123456", nombreFlujo, 1, documento, nombreDocumento);
                        ////string respppp = wf.crearExpedienteConFichero("jgonzalez@zertifika.com", "scn1006", nombreFlujo, 1, documento, nombreDocumento);
                        // masTrazas += "respuesta ws = " + resp2 + Environment.NewLine;
                        // File.WriteAllText(WebConfigurationManager.AppSettings["ruta"] + "masTrazas.txt", masTrazas);

                        ////respppp = wf.enviarInvitacionUsuario("jgonzalez@zertifika.com", "scn1006", "jgonzalez@zertifika.com",
                        ////    "Expediente: " + nombreFlujo, "Se ha creado un nuevo expediente, " + nombreFlujo +
                        ////    ", con el siguiente fichero: " + nombreDocumento + ". Puedes verlo entrando en tu cuenta de anywhere.");


                    }

                    // respppp = wf.enviarInvitacionUsuario("jgonzalez@zertifika.com", "scn1006", "josearamz@gmail.com",
                    //         "Expediente: " + nombreFlujo, "Se ha creado un nuevo expediente, " + nombreFlujo +
                    //         ", con el siguiente fichero: " + nombreDocumento + ". Puedes verlo entrando en tu cuenta de anywhere.");


                    // Finalmente miramos si es un subflujo, de ser así volvemos a activar el estado en el que se quedó
                    resp = accionesSubflujo(idFlujo, idFicha);

                    return 1;

                }
                // File.WriteAllText(WebConfigurationManager.AppSettings["ruta"] + "masTrazas.txt", masTrazas);

                return resp;
            }
            catch (Exception e)
            {
                //File.WriteAllText(WebConfigurationManager.AppSettings["ruta"] + "trazaError.txt", e.ToString());
                return -1;
            }
        }

        public static string getIdFlujo(string idPaso)
        {
            String query = "SELECT idFlujo FROM Pasos WHERE idPaso = '" + idPaso + "'";
            return DatabaseConnection.executeScalar(query, CommandType.Text, ConnectionString.WorkFlow).ToString();
        }


        public static int actualizarCampos(string tokenUsuario, String idEstado, String idFlujo, HttpContext context, string idFicha)
        {
            String query = "";
            String queryUpdate = "";
            try
            {
                string idUsuario = tokenUsuario.Split('I')[0];
                if (Utilities.comprobarAutorizacionUsuarioEstado(Convert.ToInt32(idUsuario), Convert.ToInt32(idEstado)))
                {
                    if (context != null)
                    {

                        string idPaso = DatabaseConnection.executeScalar("SELECT TOP 1 idPaso FROM Estados WHERE idEstado = '" +
                            idEstado + "'", CommandType.Text, ConnectionString.WorkFlow).ToString();

                        string nombreTabla = BasicoFlujo.getNombreTabla(idFlujo);

                        query = "SELECT c.Nombre, cp.Tipo, p.idPaso FROM Campos AS c INNER JOIN " +
                          "CamposPasos AS cp ON c.idCampo = cp.idCampo INNER JOIN " +
                          "Pasos AS p ON p.idPaso = cp.idPaso " +
                          "WHERE  (cp.Tipo = 'E') AND (p.idPaso = '" + idPaso + "')";

                        DataTable dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                        queryUpdate = "UPDATE " + nombreTabla + " set ";

                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                string nombre = Utilities.getParam(context, dr["Nombre"].ToString());

                                if (String.IsNullOrEmpty(nombre))
                                {
                                    queryUpdate += dr["Nombre"] + " = NULL, ";
                                }
                                else
                                {
                                    queryUpdate += dr["Nombre"] + " = '" + nombre + "', ";
                                }
                            }
                            // Quito la coma 
                            queryUpdate = queryUpdate.Substring(0, queryUpdate.Length - 2);
                            queryUpdate += " WHERE idFicha = '" + idFicha + "'";
                            int resp = DatabaseConnection.executeNonQueryInt(queryUpdate, CommandType.Text, ConnectionString.WorkFlow);
                            if (resp < 1) return -1;
                            workFlow.classes.Utilities.addEvento("Guardar metadatos", "Guardar metadatos ", idUsuario + "", null, idEstado);
                        }
                        return 1;
                    }
                    else
                    {
                        // el context llega como nulo, por ejemplo llamando a aprobar paso desde una acción, devolvemos 1 como si hubieramos hecho cambios 
                        return 1;
                    }
                   
                }
                return -1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }


        public static int accionesSubflujo(string idFlujo, string idFicha)
        {
            try
            {

                string query = "SELECT idEstadoPrevio FROM Subprocesos WHERE idFlujo = '" + idFlujo + "' AND idFicha = '" + idFicha + "'";
                DataTable datosSubflujos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                if (datosSubflujos.Rows.Count > 0)
                {
                    // Solo contemplo que este flujo dependa de otra, no de varios
                    int idEstadoPrevio = Convert.ToInt32(datosSubflujos.Rows[0]["idEstadoPrevio"]);
                    if (idEstadoPrevio > -1)
                    {
                        query = "UPDATE Estados SET Activo = 'True' WHERE idEstado = '" + idEstadoPrevio + "'";
                        int resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);
                        return resp;
                    }
                    else
                    {
                        return 0;
                    }
                }
                return 0;

            }
            catch (Exception e)
            {
                return -1;
            }
        }

        internal static string getJSON(string idFlujo)
        {
            string query = string.Empty;
            string resultado = string.Empty;
            try
            {
                // Obtenemos los datos del flujo
                query = "SELECT Descripcion, Activo, QR, idResponsable FROM Flujo WHERE idFlujo = " + idFlujo;
                DataTable dtFlujo = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                resultado += "infoProceso.Descripcion = \"" + dtFlujo.Rows[0]["Descripcion"] + "\";";
                if (dtFlujo.Rows[0]["Activo"].ToString() == "S")
                {
                    resultado += "infoProceso.activo = true;";
                }
                else
                {
                    resultado += "infoProceso.activo = false;";
                }

                resultado += "infoProceso.incluyeQR = " + dtFlujo.Rows[0]["QR"].ToString().ToLower() + ";";
                resultado += "infoProceso.idResponsable = \"" + dtFlujo.Rows[0]["idResponsable"] + "\";";


                query = "SELECT NumPaso, Descripcion, Usuario, TipoUsuario, " +
                    " NecesitaFirma, NumeroFirmas, Biometria, EnvioLibre, SeguimientoGPS, EnviarNotificacion, ExportarCSV, " + 
                    " pasoOK, pasoNOK, ExportarXML, idCondicion, idAccion, pasoPRE FROM Pasos " +
                    " WHERE idFlujo = " + idFlujo;
                DataTable dtEstados = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                if (dtEstados.Rows.Count > 0)
                {
                    // Me salto el cero porque es el de inicio
                    for (int i = 0; i < dtEstados.Rows.Count; i++)
                    {
                        
                        int numPaso = Convert.ToInt32(dtEstados.Rows[i]["NumPaso"]);
/*
                        resultado += "infoPasos[" + numPaso + "].descripcion = \"" + dtEstados.Rows[i]["Descripcion"] + "\";";
                        resultado += "infoPasos[" + numPaso + "].CSV = " + dtEstados.Rows[i]["ExportarCSV"].ToString().ToLower() + ";";
                        resultado += "infoPasos[" + numPaso + "].GPS = " + dtEstados.Rows[i]["SeguimientoGPS"].ToString().ToLower() + ";";
                        resultado += "infoPasos[" + numPaso + "].Notificacion = " + dtEstados.Rows[i]["EnviarNotificacion"].ToString().ToLower() + ";";
                        resultado += "infoPasos[" + numPaso + "].XML = " + dtEstados.Rows[i]["ExportarXML"].ToString().ToLower() + ";";
                        resultado += "infoPasos[" + numPaso + "].biometria = " + dtEstados.Rows[i]["Biometria"].ToString().ToLower() + ";";
                        //resultado += "infoPasos[" + numPaso + "].campo = \"" + dtEstados.Rows[i][""] + "\";";
                        resultado += "infoPasos[" + numPaso + "].certificado = " + dtEstados.Rows[i]["NecesitaFirma"].ToString().ToLower() + ";";
                        resultado += "infoPasos[" + numPaso + "].envioLibre = " + dtEstados.Rows[i]["EnvioLibre"].ToString().ToLower() + ";";
                        resultado += "infoPasos[" + numPaso + "].idAccion = \"" + dtEstados.Rows[i]["idAccion"] + "\";";
                        resultado += "infoPasos[" + numPaso + "].numFirmas = \"" + dtEstados.Rows[i]["NumeroFirmas"] + "\";";
                        //resultado += "infoPasos[" + numPaso + "].operador = \"" + dtEstados.Rows[i][""] + "\";";
                        resultado += "infoPasos[" + numPaso + "].pasoNOK = \"" + dtEstados.Rows[i]["pasoNOK"] + "\";";
                        resultado += "infoPasos[" + numPaso + "].pasoOK = \"" + dtEstados.Rows[i]["pasoOK"] + "\";";
                        resultado += "infoPasos[" + numPaso + "].pasoPRE = \"" + dtEstados.Rows[i]["pasoPRE"] + "\";";
                        resultado += "infoPasos[" + numPaso + "].tipoUsuario = \"" + dtEstados.Rows[i]["TipoUsuario"] + "\";";
                        resultado += "infoPasos[" + numPaso + "].usuarioAFirmar = \"" + dtEstados.Rows[i]["Usuario"] + "\";";
                        */

                        if (dtEstados.Rows[i]["idAccion"].ToString() == "-1" && dtEstados.Rows[i]["idCondicion"].ToString() == "-1")
                        {
                            resultado += "recargarPasoGrafico(\"" + numPaso + "\");";
                        }
                        else
                        {
                            if (dtEstados.Rows[i]["idAccion"].ToString() == "-1")
                            {
                                // Es un condicional
                                resultado += "recargarCondGrafico(\"" + numPaso + "\");";
                            }
                            else
                            {
                                resultado += "recargarEveGrafico(\"" + numPaso + "\");";

                            }

                        }


                    }
                }


                query = "SELECT idCampo, Nombre, Tipo, Tamaño FROM Campos " +
                    " WHERE idFlujo = " + idFlujo;
                DataTable dtCampos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                if (dtCampos.Rows.Count > 0)
                {

                    for (int i = 0; i < dtCampos.Rows.Count; i++)
                    {
                        resultado += "infoCampos[" + i + "] = {};"; 
                        resultado += "infoCampos[" + i + "].descripcion = \"" + dtCampos.Rows[i]["Nombre"] + "\";";
                        resultado += "infoCampos[" + i + "].tamano = \"" + dtCampos.Rows[i]["Tamaño"] + "\";";
                        resultado += "infoCampos[" + i + "].tipoCampo = \"" + dtCampos.Rows[i]["Tipo"] + "\";";

                        
                        int idCampo = Convert.ToInt32(dtCampos.Rows[i]["idCampo"]);
                        query = @"SELECT        p.NumPaso, cp.Tipo
                                    FROM            CamposPasos AS cp INNER JOIN
                                                             Pasos AS p ON p.idPaso = cp.idPaso INNER JOIN
                                                             Campos AS c ON c.idCampo = cp.idCampo
                                    WHERE        (p.idFlujo = " + idFlujo + ") AND (cp.idCampo = " + idCampo + ")";


                        DataTable dtCPasos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                        if (dtCPasos.Rows.Count > 0)
                        {
                            //resultado += "infoPasos[" + i + "].Campos = [];"; 
                            for (int cNPaso = 0; cNPaso < dtCPasos.Rows.Count; cNPaso++)
                            {
                                resultado += "infoPasos[" + dtCPasos.Rows[cNPaso]["NumPaso"] + "].Campos[" + i + "] = \"" + dtCPasos.Rows[cNPaso]["Tipo"] + "\";"; 


                            }

                        }
                        

                    }

                    resultado += "cargarCampos();";

                }

                query = "SELECT idAccion FROM AccionesFlujo " +
                    " WHERE idFlujo = " + idFlujo;
                DataTable dtAcciones = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                if (dtAcciones.Rows.Count > 0)
                {

                    for (int i = 0; i < dtAcciones.Rows.Count; i++)
                    {
                        resultado += "$(\"#acc-" + dtAcciones.Rows[i]["idAccion"] + "\").prop(\"checked\",true);"; 

                    }
                }


                return resultado;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getJSONHistorico", "WorkFlow", -1, idFlujo + "." + e.Message, e.ToString());
                return "";
            }
        }

 

        internal static string getJSONHistorico(string idFlujo, string idFicha)
        {
            string query = string.Empty;
            string resultado = string.Empty;
            try
            {
                query = "SELECT idEstado, NumPaso, FechaPasoInicio, FechaPasoFin, idUsuario, Aceptado, TipoUsuario, " + 
                    "idUsuarioAFirmar, InformacionAdicional, idCondicion, idAccion FROM Estados " +
                    "WHERE idFlujo = " + idFlujo + " AND idFicha = " + idFicha;
                DataTable dtEstados = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                if (dtEstados.Rows.Count > 0)
                {
                    // Me salto el cero porque es el de inicio
                    for (int i = 1; i < dtEstados.Rows.Count; i++)
                    {

                        int numPaso = Convert.ToInt32(dtEstados.Rows[i]["NumPaso"]);
                        if (!Convert.IsDBNull(dtEstados.Rows[i]["FechaPasoInicio"]) && Convert.IsDBNull(dtEstados.Rows[i]["FechaPasoFin"]))
                        {
                            resultado += "infoPasos[" + numPaso + "].estado = \"actual\";";
                        }
                        else
                        {
                            if (Convert.IsDBNull(dtEstados.Rows[i]["FechaPasoInicio"]) && Convert.IsDBNull(dtEstados.Rows[i]["FechaPasoFin"]))
                            {
                                resultado += "infoPasos[" + numPaso + "].estado = \"pendiente\";";
                            }
                            else
                            {
                                if (!Convert.IsDBNull(dtEstados.Rows[i]["FechaPasoInicio"]) && !Convert.IsDBNull(dtEstados.Rows[i]["FechaPasoFin"]))
                                {
                                    resultado += "infoPasos[" + numPaso + "].estado = \"gestionado\";";
                                    resultado += "infoPasos[" + numPaso + "].aceptado = \"" + dtEstados.Rows[i]["Aceptado"] + "\";";
                                }
                            }
                        }

                        if (dtEstados.Rows[i]["idAccion"].ToString() == "-1" && dtEstados.Rows[i]["idCondicion"].ToString() == "-1")
                        {
                            resultado += "recargarPasoGrafico(\"" + numPaso + "\");";
                        }
                        else
                        {
                            if (dtEstados.Rows[i]["idAccion"].ToString() == "-1")
                            {
                                // Es un condicional
                                resultado += "recargarCondGrafico(\"" + numPaso + "\");";
                            }
                            else
                            {
                                resultado += "recargarEveGrafico(\"" + numPaso + "\");";

                            }

                        }

                        
                    }
                }

                return resultado;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getJSONHistorico", "WorkFlow", -1, idFlujo + "." + idFicha + "." + e.Message, e.ToString());
                return "";
            }
        }

        internal static string cargarInfo(int idFlujo, int idFicha, int idUsuario)
        {
            int numPasoActual = 0;
            string respuesta = string.Empty;
            bool invertido = false;
            string icono = string.Empty;
            string descripcion = string.Empty;
            string color = string.Empty;
            bool esPasoActual = true;
            string query = string.Empty;

            try
            {
                // Empezamos obteniendo si este flujo tiene subflujos, de ser así los incluiremos en la query siguiente
                //string query = "SELECT idEstado FROM Estados WHERE idFlujo = '" + idFlujo + "' AND idFicha = '" + idFicha + "'";
                //DataTable dtEstados = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                //string estados = "";
                //foreach (DataRow dr in dtEstados.Rows)
                //{
                //    estados += dr["idEstado"] + ",";
                //}
                //estados = estados.Substring(0, estados.Length - 1);

                query = "SELECT Version FROM Flujo WHERE idFlujo = " + idFlujo;
                string version = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow);


                query = "SELECT idFlujo, idFicha FROM Subprocesos WHERE idEstadoPrevio in (SELECT idEstado FROM Estados WHERE idFlujo = '" + idFlujo + "' AND idFicha = '" + idFicha + "')";

                DataTable dtSubprocesos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                string querySubprocesos = "";
                foreach (DataRow dr in dtSubprocesos.Rows)
                {
                    querySubprocesos += "OR ( est.idFlujo = '" + dr["idFlujo"] + "' AND est.idFicha = '" + dr["idFicha"] + "') ";

                }

                query = @"SELECT        est.NumPaso, est.FechaPasoInicio, est.FechaPasoFin, est.idUsuario, est.Aceptado, usu.Nombre, est.DescripcionPaso, rec.FechaRechazo, est.Activo,
                         rec.idUsuario AS idUsuarioRechazo, rec.Activo AS rechazoActivo, rec.Observaciones, rec.idMotivoRechazo, est.InformacionAdicional, est.idFlujo, est.idFicha,
                         est.TipoUsuario, est.idUsuarioAFirmar 
                         FROM            Estados AS est LEFT OUTER JOIN
                                                 Usuarios AS usu ON usu.idUsuario = est.idUsuario LEFT OUTER JOIN
                                                 Rechazos AS rec ON rec.idEstado = est.idEstado
                         WHERE        ((est.idFlujo = '" + idFlujo + @"') AND (est.idFicha = '" + idFicha + @"') " + querySubprocesos + @") AND (est.NumPaso < 1000) OR
                                                 (est.idFlujo = '" + idFlujo + @"') AND (est.idFicha = '" + idFicha + @"') AND (est.NumPaso < 1000)
                         ORDER BY ISNULL(est.FechaPasoFin, '2079-06-05T23:59:00'), est.NumPaso, ISNULL(est.FechaPasoInicio, '2079-06-05T23:59:00')";

                DataTable dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                respuesta = "<ul class='timeline'>";
                foreach (DataRow dr in dt.Rows)
                {
                    numPasoActual = Convert.ToInt32(dr["NumPaso"]);
                    if (invertido)
                    {
                        respuesta += "<li class='timeline-inverted'>";
                        invertido = false;
                    }
                    else
                    {
                        respuesta += "<li>";
                        invertido = true;
                    }

                    if (Convert.ToInt32(dr["NumPaso"]) == 0 && String.IsNullOrEmpty(dr["DescripcionPaso"].ToString()))
                    {
                        icono = "<i class='fa fa-home'></i>";
                        descripcion = "Inicio flujo.";
                    }
                    else
                    {
                        switch (dr["Aceptado"].ToString())
                        {
                            case "S":
                                icono = "<i class='fa fa-check'></i>";
                                color = "success";
                                break;
                            case "N":
                                icono = "<i class='fa fa-times'></i>";
                                color = "danger";
                                break;
                            case "I":
                                icono = "<i class='fa fa-ban'></i>";
                                color = "default";
                                break;
                        }

                        descripcion = dr["DescripcionPaso"].ToString();
                    }

                    string descripcionAdicional = "";
                    if (!String.IsNullOrEmpty(dr["InformacionAdicional"].ToString()))
                    {
                        descripcionAdicional += "<i class='fa fa-info-circle'></i> " + dr["InformacionAdicional"].ToString() + "<br />";
                    }

                    string claseSubflujo = "";
                    if (!(Convert.ToInt32(dr["idFlujo"]) == idFlujo) || !(Convert.ToInt32(dr["idFicha"]) == idFicha))
                    {
                        // Si alguno de los dos es distinto le cambiamos el color
                        claseSubflujo = "subproceso";
                    }

                    esPasoActual = esPasoActualMetodo(dr);

                    if (esPasoActual)
                    {
                        icono = "<i class='fa fa-arrow-right '></i>";
                        color = "warning";
                        descripcion = dr["DescripcionPaso"].ToString();
                        esPasoActual = false;
                    }
                    else
                    {
                        // Si aún no ha sido aceptado
                        if (Convert.IsDBNull(dr["Aceptado"]))
                        {
                            icono = "<i class='fa fa-spinner fa-spin'></i>";
                            color = "default";
                            descripcion = dr["DescripcionPaso"].ToString();
                        }

                    }

                    string fechaPasoFin = dr["FechaPasoFin"].ToString();
                    if (!string.IsNullOrEmpty(fechaPasoFin)) fechaPasoFin = "<i class='fa fa-clock-o'></i>" + fechaPasoFin;

                    respuesta += @"<div class='timeline-badge " + color + "'>" + icono + @"
                                   </div>
                                    <div class='timeline-panel " + claseSubflujo + @"'>
                                        <div class='timeline-heading'>
                                            <h4 class='timeline-title'>" + descripcion + @"</h4>
                                            <p><small class='text-muted'>" + fechaPasoFin + @"</small>
                                            </p>
                                        </div>
                                        <div class='timeline-body'>
                                            <p>" + descripcionAdicional + tratarCuerpoTimeline(dr) + @"</p>
                                        </div>
                                    </div>
                                </li>";
                }


                /*
                // Cargamos el resto del timeline, con los pasos que no han pasado aun
                query = "SELECT NumPaso, InformacionAdicional, DescripcionPaso, idUsuarioAFirmar, TipoUsuario FROM Estados WHERE idFlujo = '" + 
                    idFlujo + "' AND idFicha = '" + idFicha + "' AND NumPaso > " + numPasoActual + " AND Numpaso < 1000 ORDER BY NumPaso ASC";

                dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                respuesta += "<ul class='timeline'>";
                foreach (DataRow dr in dt.Rows)
                {
                    if (invertido)
                    {
                        respuesta += "<li class='timeline-inverted'>";
                        invertido = false;
                    }
                    else
                    {
                        respuesta += "<li>";
                        invertido = true;
                    }

                    string descripcionAdicional = "";
                    if (!String.IsNullOrEmpty(dr["InformacionAdicional"].ToString()))
                    {
                        descripcionAdicional += "<i class='fa fa-info-circle'></i> " + dr["InformacionAdicional"].ToString() + "<br />";
                    }

                    if (esPasoActual)
                    {
                        icono = "<i class='fa fa-arrow-right '></i>";
                        color = "warning";
                        descripcion = dr["DescripcionPaso"].ToString();
                        esPasoActual = false;
                    }
                    else
                    {
                        icono = "<i class='fa fa-spinner fa-spin'></i>";
                        color = "default";
                        descripcion = dr["DescripcionPaso"].ToString();
                        
                    }

                    respuesta += @"<div class='timeline-badge " + color + "'>" + icono + @"
                                   </div>
                                    <div class='timeline-panel'>
                                        <div class='timeline-heading'>
                                            <h4 class='timeline-title'>" + descripcion + @"</h4>         
                                        </div>
                                        <div class='timeline-body'>
                                            <p>" + descripcionAdicional + tratarCuerpoTimelineProximos(dr) + @"</p>
                                        </div>
                                    </div>
                                </li>";


                }
                */
                // Añado el de fin si está finalizado
                //if (flujoFinalizado)
                //{
                if (invertido)
                {
                    respuesta += "<li class='timeline-inverted'>";
                    invertido = false;
                }
                else
                {
                    respuesta += "<li>";
                    invertido = true;
                }

                respuesta += @"<div class='timeline-badge default'><i class='fa fa-flag-checkered'></i></div></li>";
                //}



                if (version == "2")
                {

                    respuesta += "<a href='javascript:abrirModeladoProceso()'>Ver modelo del proceso </a>";

                    // Es la nueva versión
                    // Obtenemos el JSON
                    string path = BasicoFlujo.getRutaJSONFlujo(idFlujo + "");

                    if (File.Exists(path))
                    {
                        string JSON = File.ReadAllText(path);

                        string JSONHistorico = BasicoFlujo.getJSONHistorico(idFlujo + "", idFicha + "");

                        respuesta += @"
                            <div class='modal fade' id='modalModeladoProceso'>
                              <div class='modal-dialog'>
                                <div class='modal-content'>
                                  <div class='modal-header'>
                                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                    <h4 class='modal-title'>Modelado del proceso</h4>
                                  </div>
                                  <div class='modal-body'>
                                  
                        <div id='drawingArea' style=''>
                            <div class='startpoint windowW point node' style='left: 150px; top:20px' data-nodetype='startpoint' id='startpoint'>Inicio</div>
    
                            <div class='windowW task' style='left: 120px; top:200px; display:none; text-align:center;' data-nodetype='task' id='taskcontainer0'>
                                <div class='ctrl_container'>
           
                                </div>
                                <div class='details_container'>
                                    <label class='detail_label'>NOMBRE DEL PASO</label>
                                    <!--<input value='' class='detail_text'/>-->
                                </div>
                            </div>

                            <div class='windowW task' style='left: 120px; top:200px; display:none; text-align:center;' data-nodetype='event' id='eventcontainer0'>
                                <div class='ctrl_container'>
            
                                </div>
                                <div class='details_container'>
                                    <label class='detail_label'>NOMBRE DEL EVENTO</label>

                                    <!--<input value='' class='detail_text'/>-->
                                </div>
                            </div>
    
                            <div class='windowW decision' style='left: 250px; top:300px; display:none; text-align:center;' data-nodetype='decision' id='decisioncontainer0'>
                                <label class='detail_labelCond'>CONDICION</label>
                                <div class='ctrl_container'>
            
                                </div>
                            </div>
    
                            <div class='windowW endpoint pointFin node' style='left: 150px; top:400px' data-nodetype='endpoint' id='endpoint'>Fin</div>
                        </div>

                    </div>
                                  <div class='modal-footer'>
                                    <button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>
                                  </div>
                                </div><!-- /.modal-content -->
                              </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->



                        <script>var flowChartJson = " + JSON + "; var JSONHistorico = '" + JSONHistorico + "';</script>";



                    }
                    else
                    {
                        Trazas tFich = new Trazas("infoflujo", "index.aspx.cs", Convert.ToInt32(idUsuario), "Fichero json no encontrado", "No encontrado");
                    }
                }


                return respuesta;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("infoflujo", "index.aspx.cs", Convert.ToInt32(idUsuario), e.ToString(), e.Message);
                return "Error cargando información del flujo.";
            }
        }



        internal static string tratarCuerpoTimeline(DataRow dr)
        {
            string respuesta = "";

            try
            {
                if (Convert.IsDBNull(dr["Aceptado"]))
                {
                    if (dr["TipoUsuario"].Equals("G"))
                    {
                        string query = "SELECT TOP 1 Descripcion FROM Grupos WHERE idGrupo = '" + dr["idUsuarioAFirmar"] + "'";
                        string descripcionGrupo = DatabaseConnection.executeScalar(query, CommandType.Text, ConnectionString.WorkFlow).ToString();
                        respuesta += "Usuario: " + descripcionGrupo;
                    }
                    else
                    {
                        if (dr["TipoUsuario"].Equals("U"))
                        {
                            string query = "SELECT TOP 1 Nombre FROM Usuarios WHERE idUsuario = '" + dr["idUsuarioAFirmar"] + "'";
                            string descripcionUsuario = DatabaseConnection.executeScalar(query, CommandType.Text, ConnectionString.WorkFlow).ToString();
                            respuesta += "Usuario: " + descripcionUsuario;
                        }
                        else
                        {
                            // Es una condición
                            respuesta += "Condicion.";
                        }
                    }

                }
                else
                {

                    switch (dr["Aceptado"].ToString())
                    {
                        case "S":
                            if (Convert.ToInt32(dr["NumPaso"]) == 0) respuesta += "Paso iniciado por " + dr["Nombre"];
                            else respuesta += "Paso aceptado por " + dr["Nombre"];
                            break;
                        case "N":
                            respuesta += "Paso rechazado por " + dr["Nombre"] + "</br>";
                            //respuesta += "Motivo: " + dr["DescripcionRechazo"] + "</br>";
                            if (!String.IsNullOrEmpty(dr["Observaciones"].ToString()))
                            {
                                respuesta += "Observaciones: " + dr["Observaciones"].ToString();
                            }

                            break;

                    }

                }
                return respuesta;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("tratarCuerpoTimeline", "WorkFlow", -1, dr.ToString() + "." + e.ToString(), e.InnerException + "");
                return "";
            }

        }

        internal static bool esPasoActualMetodo(DataRow dr)
        {
            bool loEs = false;
            try
            {
                if (!Convert.IsDBNull(dr["FechaPasoInicio"]) && Convert.IsDBNull(dr["FechaPasoFin"]) && Convert.ToBoolean(dr["Activo"]))
                {
                    loEs = true;
                }
                return loEs;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("esPasoActualMetodo", "WorkFlow", -1, dr.ToString() + "." + e.ToString(), e.InnerException + "");
                return loEs;
            }
        }



        internal static string getRutaJAR(string idAccion)
        {
            string path = "00000000".Substring(idAccion.Length) + idAccion;

            if (Convert.ToInt32(idAccion) < 10) idAccion = "0" + idAccion;

            string pathString = WebConfigurationManager.AppSettings["ruta"] + "jarFiles/" + path.Substring(0, 2) + "/" + path.Substring(2, 2) + "/" +
                    path.Substring(4, 2) + "/" + idAccion.Substring(idAccion.Length - 2, 2) + ".jar";

            return pathString;
        }

        internal static string getRutaCarpetaJAR(string idAccion)
        {
            string path = "00000000".Substring(idAccion.Length) + idAccion;

            string pathString = WebConfigurationManager.AppSettings["ruta"] + "jarFiles/" + path.Substring(0, 2) + "/" + path.Substring(2, 2) + "/" +
                    path.Substring(4, 2) + "/";

            return pathString;
        }

        internal static string getRutaPublicaJAR(string idAccion)
        {
            string path = "00000000".Substring(idAccion.Length) + idAccion;

            if (Convert.ToInt32(idAccion) < 10) idAccion = "0" + idAccion;

            string pathString = "/jarFiles/" + path.Substring(0, 2) + "/" + path.Substring(2, 2) + "/" +
                    path.Substring(4, 2) + "/" + idAccion.Substring(idAccion.Length - 2, 2) + ".jar";

            return pathString;
        }


        internal static string insertarDocumentoUsuario(int idUsuario, int idUsuarioComparte, string carpeta, int idDoc)
        {
            try
            {
                string query = "INSERT INTO DocumentosUsuarios " +
                    "(iddocumento, idusuario, carpetadocumento, idusuariocomparte, FechaInclusion) " + 
                    "VALUES " + 
                    "(" + idDoc + ", " + idUsuario + ", '" + carpeta + "', " + idUsuarioComparte + ", GETDATE() )";

                int resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);

                int idEmpresa = BasicoFlujo.getIdEmpresa(idUsuario);

                query = "UPDATE Documentos SET idempresa = " + idEmpresa + " WHERE idDocumento = " + idDoc + " AND idUsuario = " + idUsuario;
                resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);

                return resp + "";
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("insertarDocumentoUsuario", "WorkFlow", idUsuario, carpeta + " . " + idDoc + " . " + e.ToString(), e.InnerException + "");
                return "-1";
            }
        }

        internal static string getIdDocAdjunto(string idDoc)
        {
            try
            {
                string query = "SELECT TOP 1 idDocAdjunto FROM DocumentosAdjuntos WHERE idDocumento = '" +
                    idDoc + "' ";
                string idDocumentoAdjunto = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow).ToString();
                return idDocumentoAdjunto;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getIdDocAdjunto", "WorkFlow", -1, idDoc + " . " + e.ToString(), e.InnerException + "");
                return "-1";
            }
        }

        internal static string getRutaFormulario(string idFormulario)
        {
            string path = "00000000".Substring(idFormulario.Length) + idFormulario;
            if (Convert.ToInt32(idFormulario) < 10) idFormulario = "0" + idFormulario;

            string pathString = WebConfigurationManager.AppSettings["ruta"] + "Formularios/" + path.Substring(0, 2) + "/" + path.Substring(2, 2) + "/" +
                    path.Substring(4, 2) + "/" + idFormulario.Substring(idFormulario.Length - 2, 2) + ".pdf";

            return pathString;
        }

        internal static string cambiarNombreADocumentoPrincipal(string idDoc, string nuevoNombre, int idUsuario)
        {
            string query = string.Empty;
            string nombreAnterior = string.Empty;
            int resp = -1;
            try
            {
                nombreAnterior = DatabaseConnection.executeScalarString(
                    "SELECT TOP 1 docOriginal FROM Documentos WHERE idDocumento = " + idDoc, 
                    CommandType.Text, 
                    ConnectionString.WorkFlow
                    );

                query = "UPDATE Documentos SET docOriginal = '" + nuevoNombre + "' WHERE idDocumento = " + idDoc;
                resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);
                if (resp == 1)
                {
                    // Guardamos evento
                    query = "INSERT INTO Eventos (Titulo, Texto, idUsuario, Fecha, idDoc) " + 
                            "VALUES ('Cambiar nombre documento principal'," + 
                            "'Cambiar nombre de: " + nombreAnterior + " a: " + nuevoNombre + "', " + idUsuario + ", GETDATE(), " + idDoc + ")";
                    resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);

                }
                return resp + "";
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("cambiarNombreADocumentoPrincipal", "WorkFlow", -1, idDoc + " . " + nuevoNombre + " . " + 
                    e.ToString(), e.InnerException + "");
                return "-1";
            }
        }
        
        internal static string DataTableToJSON(DataTable dt)
        {
            try
            {
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        if (col.DataType.Name.Equals("DateTime"))
                        {

                            if (dr[col] != null && !Convert.IsDBNull(dr[col]) && !string.IsNullOrEmpty(dr[col] + ""))
                            {
                                DateTime dtime = new DateTime();
                                dtime = Convert.ToDateTime(dr[col]);

                                row.Add(col.ColumnName, dtime.ToString("dd/MM/yyyy"));
                            }
                            else
                            {
                                row.Add(col.ColumnName, null);
                            }

                            
                        }
                        else
                        {
                            row.Add(col.ColumnName, dr[col]);
                        }
                    }
                    rows.Add(row);
                }
                return serializer.Serialize(rows);

            }
            catch (Exception e)
            {
                return "[]";
            }
        }
    }
}