﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

using System.Net;
using System.IO;

using System.Diagnostics;
using System.Text;

namespace workFlow.classes
{
    public class Acciones
    {
        string respuesta = string.Empty;

        public Acciones()
        {

        }

        public Acciones(int idEstado, int idUsuario, string tokenUsuario)
        {

            try
            {
                string query = "SELECT idAccion FROM AccionesEstado WHERE idEstado = '" + idEstado + "'";
                DataTable dtAcciones = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                for (int i = 0; i < dtAcciones.Rows.Count; i++)
                {
                    // Recorremos las acciones de este determinado flujo, buscamos por id del flujo
                    int idAccion = Convert.ToInt32(dtAcciones.Rows[i]["idAccion"]);
                    respuesta = realizarAccion(idAccion, idUsuario, idEstado, tokenUsuario);

                }
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("Acciones", "WorkFlow", -1, idEstado + "." + idUsuario + "."
                    + e.ToString(), e.InnerException + "");
            }
        }

        internal string realizarAcciones(int idEstado, int idUsuario, string tokenUsuario)
        {
            try
            {
                string query = "SELECT idAccion FROM AccionesEstado WHERE idEstado = '" + idEstado + "'";
                DataTable dtAcciones = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                for (int i = 0; i < dtAcciones.Rows.Count; i++)
                {
                    // Recorremos las acciones de este determinado flujo, buscamos por id del flujo
                    int idAccion = Convert.ToInt32(dtAcciones.Rows[i]["idAccion"]);
                    respuesta = realizarAccion(idAccion, idUsuario, idEstado, tokenUsuario);

                }
                return respuesta;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("Acciones", "WorkFlow", -1, idEstado + "." + idUsuario + "."
                    + e.ToString(), e.InnerException + "");
                return "-1";
            }
        }

            internal string realizarAccion(int idAccion, int idUsuario, int idEstado, string tokenUsuario)
            {
                string respuesta = "";
                try
                {
                    switch (idAccion)
                    {
                        case 2:
                        case 7:
                            respuesta = ejecutarAccionAdjuntarFormularioYPonerPrincipal(idUsuario, idEstado, tokenUsuario);
                            break;

                        case 3:
                            respuesta = ejecutarAccionObtenerDatosActivo(idUsuario, idEstado, tokenUsuario, idAccion);
                            break;

                        case 4:
                            respuesta = ejecutarAccionIniciarSubflujo(idUsuario, idEstado, tokenUsuario, idAccion);
                            break;

                        case 8:
                            respuesta = ejecutarAccionFirmarDocumento(idUsuario, idEstado, tokenUsuario, idAccion);
                            break;

                        default:
                            respuesta = ejecutarJAR(idUsuario, idEstado, tokenUsuario, idAccion);
                            break;

                    }
                    this.respuesta = respuesta;
                    workFlow.classes.Utilities.addEvento("Accion ejecutada", "Accion ejecutada. idEstado: " + idEstado + " idAccion: " + idAccion + " Respuesta: " + respuesta, idUsuario + "");
                    return respuesta;
                }
                catch (Exception e)
                {
                    Trazas t = new Trazas("realizarAccion", "WorkFlow", -1, idEstado+ "." + idUsuario + "."
                        + e.ToString(), e.InnerException + "");
                    return "-1";
                }

            }

            private string ejecutarAccionFirmarDocumento(int idUsuario, int idEstado, string tokenUsuario, int idAccion)
            {
                string query = string.Empty;
                string respuesta = string.Empty;

                try
                {
                    query = "SELECT TOP 1 idFlujo, idFicha FROM Estados WHERE idEstado = " + idEstado + "";
                    DataTable dtDatos = DatabaseConnection.executeNonQueryDT(query,
                        CommandType.Text, ConnectionString.WorkFlow);

                    string idFlujo = dtDatos.Rows[0]["idFlujo"].ToString();
                    string idFicha = dtDatos.Rows[0]["idFicha"].ToString();

                    int idDoc = Convert.ToInt32(BasicoFlujo.getIdDocumento(idFlujo, idFicha));

                    if (idDoc > 0)
                    {


                    }

                   

                    return respuesta;
                }
                catch (Exception e)
                {
                    Trazas t = new Trazas("ejecutarAccionFirmarDocumento", "WorkFlow", -1, idEstado + "." + idUsuario + "."
                        + e.ToString(), e.InnerException + "");
                    return "-1";
                }
            }

            private string ejecutarAccionIniciarSubflujo(int idUsuario, int idEstado, string tokenUsuario, int idAccion)
            {
                string respuesta = string.Empty;
                string query = string.Empty;
                try
                {
                    query = "SELECT TOP 1 idFlujo, idFicha FROM Estados WHERE idEstado = " + idEstado + "";
                    DataTable dtDatos = DatabaseConnection.executeNonQueryDT(query,
                        CommandType.Text, ConnectionString.WorkFlow);

                    string idFlujo = dtDatos.Rows[0]["idFlujo"].ToString();
                    string idFicha = dtDatos.Rows[0]["idFicha"].ToString();

                    int idFlujoSubproceso = 2;
                    string descripcion = "Subproceso";

                    return "1";

                }
                catch (Exception e)
                {
                    Trazas t = new Trazas("ejecutarAccionIniciarSubflujo", "WorkFlow", idUsuario, idEstado + "." + tokenUsuario + "."
                        + e.ToString(), e.InnerException + "");
                    return "-1";
                }

            }

            private string ejecutarAccionObtenerDatosActivo(int idUsuario, int idEstado, string tokenUsuario, int idAccion)
            {
                string respuesta = string.Empty;
                string query = string.Empty;
                try
                {

                    // OBTENEMOS EL CONTENIDO DEL CAMPO ASIGNADO DONDE ESTARÁN LOS PARÁMETROS DE ESTA FUNCIÓN
                    query = "SELECT TOP 1 idFlujo, idFicha FROM Estados WHERE idEstado = " + idEstado + "";
                    DataTable dtDatos = DatabaseConnection.executeNonQueryDT(query,
                        CommandType.Text, ConnectionString.WorkFlow);

                    string idFlujo = dtDatos.Rows[0]["idFlujo"].ToString();
                    string idFicha = dtDatos.Rows[0]["idFicha"].ToString();

                    query = "SELECT TOP 1 Campo FROM AccionesFlujo WHERE idFlujo = " + idFlujo + " AND idAccion = " + idAccion;
                    string campo = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow);

                    // En el valor del campo tenemos el idActivo

                    int idActivo = Convert.ToInt32(BasicoFlujo.getValueCampo(campo, idFlujo, idFicha));

                    // Obtenemos los datos
                    query = "SELECT TOP 1 Descripcion, Marca, Modelo, " +
                            "Serie, FechaUltimaRevision, Departamento " +
                            "FROM Activos " +
                            "WHERE idActivo = " + idActivo;

                    DataTable dtActivo = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.tmpActivos);

                    if (dtActivo.Rows.Count > 0)
                    {
                        query = "UPDATE f00000003 SET " +
                            " DESCRIPCION1 = '" + dtActivo.Rows[0]["Descripcion"] + "'," +
                            " MARCA1 = '" + dtActivo.Rows[0]["Marca"] +  "'," +
                            " MODELO1 = '" + dtActivo.Rows[0]["Modelo"] + "'," +
                            " SERIE1 = '" + dtActivo.Rows[0]["Serie"] + "' WHERE idFicha = " + idFicha;

                        int resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);

                    }

                    return "1";

                }
                catch (Exception e)
                {
                    Trazas t = new Trazas("ejecutarAccionObtenerDatosActivo", "WorkFlow", -1, idEstado + "." + idUsuario + "."
                        + e.ToString(), e.InnerException + "");
                    return "-1";
                }

            }

            private string ejecutarAccionAdjuntarFormularioYPonerPrincipal(int idUsuario, int idEstado, string tokenUsuario)
            {
                string respuesta = string.Empty;
                string query = string.Empty;
                try
                {

                    DataTable dtDatos = DatabaseConnection.executeNonQueryDT("SELECT TOP 1 idFlujo, idFicha, NumPaso FROM Estados WHERE idEstado = '" + idEstado + "'",
                    CommandType.Text, ConnectionString.WorkFlow);

                    string idFlujo = dtDatos.Rows[0]["idFlujo"].ToString();
                    string idFicha = dtDatos.Rows[0]["idFicha"].ToString();
                    int numPaso = Convert.ToInt32(dtDatos.Rows[0]["NumPaso"]);

                    string idDoc = BasicoFlujo.getIdDocumento(idFlujo, idFicha);



                    // Miramos si está aprobado o no
                    if (idFlujo == "2")
                    {
                    }
                    else
                    {

                        if (idFlujo == "3")
                        {


                            // es el flujo 3
                            if (numPaso == 3)
                            {
                              
                            }
                            else
                            {


                               
                            }

                        }
                        else
                        {
                            // Es el flujo 4
                           


                        }


                    }

                    // Obtengo el doc adjunto 
                    string idDocAdjunto = BasicoFlujo.getIdDocAdjunto(idDoc);


                    return respuesta;

                }
                catch (Exception e)
                {
                    Trazas t = new Trazas("ejecutarAccionAdjuntarFormularioYPonerPrincipal", "WorkFlow", -1, idEstado + "." + idUsuario + "."
                        + e.ToString(), e.InnerException + "");
                    return "-1";
                }
            }

            private string ejecutarJAR(int idUsuario, int idEstado, string tokenUsuario, int idAccion)
            {
                string query = string.Empty;
                string respuesta = string.Empty;
                try
                {
                    // OBTENEMOS EL CONTENIDO DEL CAMPO ASIGNADO DONDE ESTARÁN LOS PARÁMETROS DE ESTA FUNCIÓN
                    query = "SELECT TOP 1 idFlujo, idFicha FROM Estados WHERE idEstado = " + idEstado + "";
                    DataTable dtDatos = DatabaseConnection.executeNonQueryDT(query,
                        CommandType.Text, ConnectionString.WorkFlow);

                    string idFlujo = dtDatos.Rows[0]["idFlujo"].ToString();
                    string idFicha = dtDatos.Rows[0]["idFicha"].ToString();

                    query = "SELECT TOP 1 Campo FROM AccionesFlujo WHERE idFlujo = " + idFlujo + " AND idAccion = " + idAccion;
                    string campo = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow);

                    string valorCampo = string.Empty;
                    if (!string.IsNullOrEmpty(campo))
                    {

                        valorCampo = BasicoFlujo.getValueCampo(campo, idFlujo, idFicha);

                        // Además sustuitimos todos los %NOMBRE_CAMPO% por el valor verdadero
                        if (valorCampo.Contains('%'))
                        {
                            // Obtenemos todos los campos del flujo
                            DataTable dtCampos = DatabaseConnection.executeNonQueryDT("SELECT Nombre FROM Campos WHERE idFlujo = " + idFlujo, CommandType.Text, ConnectionString.WorkFlow);
                            // Recorremos estos campos y si estuvieran sustituimos su valor
                            for (int i = 0; i < dtCampos.Rows.Count; i++)
                            {
                                string nombreCampo = dtCampos.Rows[i]["Nombre"].ToString();
                                if (valorCampo.Contains("%" + nombreCampo + "%"))
                                {
                                    string valorDeEsteCampo = BasicoFlujo.getValueCampo(nombreCampo, idFlujo, idFicha);
                                    valorCampo = valorCampo.Replace("%" + nombreCampo + "%", valorDeEsteCampo);
                                }
                            }
                        }
                    }

                    string rutaJar = BasicoFlujo.getRutaJAR(idAccion + "");
                    if (File.Exists(rutaJar))
                    {
                        var processInfo = new ProcessStartInfo("java.exe", "-jar " + rutaJar + " " + valorCampo)
                        {
                            CreateNoWindow = false,
                            UseShellExecute = false,
                            RedirectStandardOutput = true
                        };
                        Process proc;

                        proc = Process.Start(processInfo);

                        //string strOutput = proc.StandardOutput.ReadToEnd();

                        //p.WaitForExit();
                        StringBuilder value = new StringBuilder();
                        while (!proc.HasExited)
                        {
                            value.Append(proc.StandardOutput.ReadToEnd());
                        }
                        respuesta = value.ToString();


                        proc.WaitForExit();
                        int exitCode = proc.ExitCode;
                        proc.Close();
                        // Limpiamos la respuesta
                        respuesta = Utilities.tratarParam(respuesta);
                        // GUARDAMOS EN ESE CAMPO LA RESPUESTA, SI EXISTE ESE CAMPO
                        if (!string.IsNullOrEmpty(campo))
                        {
                            query = "UPDATE " + BasicoFlujo.getNombreTabla(idFlujo) + " SET " + campo + " = '" + respuesta + "' WHERE idFicha = " + idFicha;
                            int resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);
                        }
                    }

                    return respuesta;
                }
                catch (Exception e)
                {
                    Trazas t = new Trazas("ejecutarJAR", "WorkFlow", idUsuario, idEstado + "." + idAccion + "."
                       + e.ToString(), e.InnerException + "");
                    return "-1";
                }
            }


            internal string getRespuesta()
            {
                return this.respuesta;
            }
    }



    
}