﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using workFlow.classes;
using System.Data;

namespace workFlow.classes
{
    public class Notificacion
    {
        string Titulo;
        string Descripcion;
        int idUsuarioEmisor, idUsuarioReceptor, idEstado;
        string FechaCreacion;
        string FechaVisto;
        string Tipo;
        int idFlujo, idFicha;
        public Notificacion()
        {
            this.Titulo = "";
            this.Descripcion = "";
            this.idUsuarioEmisor = 0;
            this.idUsuarioReceptor = 0;
            this.FechaCreacion = "";
            this.FechaVisto = "";
            this.Tipo = "";
            this.idFlujo = 0;
            this.idFicha = 0;
            this.idEstado = 0;
        }



        public void setNotificacionNuevaInformacion(int idUsuario, int idFlujo, int idFicha)
        {
            try
            {

                this.idUsuarioEmisor = idUsuario;
                this.idFlujo = idFlujo;
                this.idFicha = idFicha;

                string nombreDoc = DatabaseConnection.executeScalar("SELECT TOP 1 docOriginal FROM Documentos WHERE idFlujo = '" + idFlujo + "' AND idFicha = '" + idFicha + "' ",
                    CommandType.Text, ConnectionString.WorkFlow).ToString();

                string nombreUsuario = DatabaseConnection.executeScalar("SELECT TOP 1 Nombre FROM Usuarios WHERE idUsuario = '" + idUsuario + "'",
                    CommandType.Text, ConnectionString.WorkFlow).ToString();


                //this.Descripcion = nombreUsuario + " ha actualizado los metadatos de " + nombreDoc;
                this.Descripcion = nombreUsuario + " te ha enviado una notificación.";
                this.idFlujo = idFlujo;
                this.idFicha = idFicha;

                DataTable dtUsuarios = DatabaseConnection.executeNonQueryDT("SELECT TOP 1 idUsuario FROM Estados WHERE idFlujo = '" + idFlujo + "' AND idFicha = '" +
                    idFicha + "' ORDER BY NumPaso ASC", CommandType.Text, ConnectionString.WorkFlow);

                // Enviamos esta notificación a todos los que han aprobado algún paso de este flujo
                foreach (DataRow dr in dtUsuarios.Rows)
                {
                    this.idUsuarioReceptor = Convert.ToInt32(dr["idUsuario"]);
                    this.FechaCreacion = DateTime.Now.ToString();
                    this.Tipo = "6";
                    insertarTraza();
                }

                // Obtenemos los idEstados del paso actual
                DataTable dtEstados = DatabaseConnection.executeNonQueryDT("SELECT TOP 1 idEstado, TipoUsuario, idUsuarioAFirmar FROM Estados WHERE idFlujo = '" + 
                    idFlujo + "' AND idFicha = '" + idFicha + "' AND NumPaso < 1000 AND FechaPasoFin IS NULL ORDER BY NumPaso ASC", CommandType.Text, ConnectionString.WorkFlow);

                if (dtEstados.Rows.Count > 0)
                {
                    // Si entra aquí es que hay pasos por aprobar, ahora notificamos a los que tienen el paso actual

                    char tUsuario = Convert.ToChar(dtEstados.Rows[0]["TipoUsuario"]);
                    int idUsuarioAFirmar = Convert.ToInt32(dtEstados.Rows[0]["idUsuarioAFirmar"]);
                    if (tUsuario == 'U')
                    {
                        this.idUsuarioReceptor = idUsuarioAFirmar;
                        this.FechaCreacion = DateTime.Now.ToString();
                        this.Tipo = "1";
                        insertarTraza();
                    }
                    else
                    {
                        // Es un grupo, se lo enviamos a todos
                        DataTable dtUsuariosGrupo = DatabaseConnection.executeNonQueryDT("SELECT idUsuario FROM UsuariosGrupos WHERE idGrupo = '" + idUsuarioAFirmar + "'", CommandType.Text, ConnectionString.WorkFlow);
                        foreach (DataRow dr in dtUsuariosGrupo.Rows)
                        {
                            this.idUsuarioReceptor = Convert.ToInt32(dr["idUsuario"]);
                            this.FechaCreacion = DateTime.Now.ToString();
                            this.Tipo = "1";
                            insertarTraza();
                        }
                    }

                } 
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("setNotificacionNuevaInformacion", "Notificacion", -1, idFlujo + "." + idFicha + "." + e.ToString(), e.InnerException + "");
            }
        }

        public void setNotificacionFlujoRechazado(int idFlujo, int idFicha, int idUsuario)
        {
            try
            {

                this.idUsuarioEmisor = idUsuario;

                string nombreDoc = DatabaseConnection.executeScalar("SELECT TOP 1 docOriginal FROM Documentos WHERE idFlujo = '" + idFlujo + "' AND idFicha = '" + idFicha + "' ",
                    CommandType.Text, ConnectionString.WorkFlow).ToString();

                string nombreUsuario = DatabaseConnection.executeScalar("SELECT TOP 1 Nombre FROM Usuarios WHERE idUsuario = '" + idUsuario + "'",
                    CommandType.Text, ConnectionString.WorkFlow).ToString();


                this.Descripcion = nombreUsuario + "ha rechazado el documento " + nombreDoc;
                this.idFlujo = idFlujo;
                this.idFicha = idFicha;

                DataTable dtUsuarios = DatabaseConnection.executeNonQueryDT("SELECT TOP 1 idUsuario FROM Estados WHERE idFlujo = '" + idFlujo + "' AND idFicha = '" +
                    idFicha + "' AND idUsuario <> " + idUsuario + " ORDER BY NumPaso ASC", CommandType.Text, ConnectionString.WorkFlow);

                foreach (DataRow dr in dtUsuarios.Rows)
                {
                    this.idUsuarioReceptor = Convert.ToInt32(dr["idUsuario"]);
                    this.FechaCreacion = DateTime.Now.ToString();
                    this.Tipo = "3";
                    insertarTraza();
                }


            }
            catch (Exception e)
            {
                Trazas t = new Trazas("setNotificacionFlujoTerminado", "Notificacion", -1, idFlujo + "." + idFicha + "." + e.ToString(), e.InnerException + "");
            }
        }


        public void setNotificacionFlujoAprobado(int idFlujo, int idFicha, int idUsuario)
        {
            try
            {

                this.idUsuarioEmisor = idUsuario;

                object objNombreDoc = DatabaseConnection.executeScalar("SELECT TOP 1 docOriginal FROM Documentos WHERE idFlujo = '" + idFlujo + "' AND idFicha = '" + idFicha + "' ",
                    CommandType.Text, ConnectionString.WorkFlow);

                

                string nombreUsuario = DatabaseConnection.executeScalar("SELECT TOP 1 Nombre FROM Usuarios WHERE idUsuario = '" + idUsuario + "'",
                    CommandType.Text, ConnectionString.WorkFlow).ToString();


                if (objNombreDoc != null)
                {
                    this.Descripcion = nombreUsuario + " ha aprobado el documento " + objNombreDoc.ToString() ;
                }
                else
                {
                    this.Descripcion = nombreUsuario + " ha aprobado un documento sin nombre";
                }

                
                this.idFlujo = idFlujo;
                this.idFicha = idFicha;

                // Obtenemos los usuarios que ya han aprobado ese flujo y les notificamos que un paso se ha aprobado
                DataTable dtUsuarios = DatabaseConnection.executeNonQueryDT("SELECT TOP 1 idUsuario FROM Estados WHERE idFlujo = '" + idFlujo + "' AND idFicha = '" +
                    idFicha + "' AND FechaPasoFin IS NOT NULL ORDER BY NumPaso ASC", CommandType.Text, ConnectionString.WorkFlow);

                

                foreach (DataRow dr in dtUsuarios.Rows)
                {
                    this.idUsuarioReceptor = Convert.ToInt32(dr["idUsuario"]);
                    this.FechaCreacion = DateTime.Now.ToString();
                    this.Tipo = "2";
                    insertarTraza();
                }


            }
            catch (Exception e)
            {
                Trazas t = new Trazas("setNotificacionFlujoTerminado", "Notificacion", -1, idFlujo + "." + idFicha + "." + e.ToString(), e.InnerException + "");
            }
        }


        public void setNotificacionFlujoTerminado(int idFlujo, int idFicha, int idUsuario)
        {
            try
            {

                this.idUsuarioEmisor = idUsuario;

                string nombreDoc = DatabaseConnection.executeScalar("SELECT TOP 1 docOriginal FROM Documentos WHERE idFlujo = '" + idFlujo + "' AND idFicha = '" + idFicha + "' ",
                    CommandType.Text, ConnectionString.WorkFlow).ToString();

                this.Descripcion = "Flujo terminado. " + nombreDoc;
                this.idFlujo = idFlujo;
                this.idFicha = idFicha;

                DataTable dtUsuarios = DatabaseConnection.executeNonQueryDT("SELECT TOP 1 idUsuario FROM Estados WHERE idFlujo = '" + idFlujo + "' AND idFicha = '" +
                    idFicha + "' ORDER BY NumPaso ASC", CommandType.Text, ConnectionString.WorkFlow);

                foreach (DataRow dr in dtUsuarios.Rows)
                {
                    this.idUsuarioReceptor = Convert.ToInt32(dr["idUsuario"]);
                    this.FechaCreacion = DateTime.Now.ToString();
                    this.Tipo = "5";
                    insertarTraza();
                }
                

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("setNotificacionFlujoTerminado", "Notificacion", -1, idFlujo + "." + idFicha + "." + e.ToString(), e.InnerException + "");
            }
        }


        public void setNotificacionNuevoDocumento(int idFlujo, int idFicha, int idUsuario, int idEstado)
        {
            try
            {

                this.idUsuarioEmisor = idUsuario;

                object nombreDoc = DatabaseConnection.executeScalar("SELECT TOP 1 docOriginal FROM Documentos WHERE idFlujo = '" + idFlujo + "' AND idFicha = '" + idFicha + "' ", 
                    CommandType.Text, ConnectionString.WorkFlow);

                if (nombreDoc != null)
                {
                    this.Descripcion = "Nuevo documento " + nombreDoc.ToString();
                }
                else
                {
                    this.Descripcion = "Nuevo documento. ";
                }
                
                this.idFlujo = idFlujo;
                this.idFicha = idFicha;
                this.idEstado = idEstado;

                DataTable dtUsuarios = DatabaseConnection.executeNonQueryDT("SELECT TOP 1 idUsuarioAFirmar, TipoUsuario FROM Estados WHERE idEstado = '" + idEstado + "' ORDER BY NumPaso ASC", CommandType.Text, ConnectionString.WorkFlow);

                char tUsuario = Convert.ToChar(dtUsuarios.Rows[0]["TipoUsuario"]);
                int idUsuarioAFirmar = Convert.ToInt32(dtUsuarios.Rows[0]["idUsuarioAFirmar"]);
                if (tUsuario == 'U')
                {
                    this.idUsuarioReceptor = idUsuarioAFirmar;
                    this.FechaCreacion = DateTime.Now.ToString();
                    this.Tipo = "1";
                    insertarTraza();
                }
                else
                {
                    // Es un grupo, se lo enviamos a todos
                    DataTable dtUsuariosGrupo = DatabaseConnection.executeNonQueryDT("SELECT idUsuario FROM UsuariosGrupos WHERE idGrupo = '" + idUsuarioAFirmar + "'", CommandType.Text, ConnectionString.WorkFlow);
                    foreach (DataRow dr in dtUsuariosGrupo.Rows)
                    {
                        this.idUsuarioReceptor = Convert.ToInt32(dr["idUsuario"]);
                        this.FechaCreacion = DateTime.Now.ToString();
                        this.Tipo = "1";
                        insertarTraza();
                    }
                }
 
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("setNotificacionFlujo", "Notificacion", -1, idFlujo + "." + idFicha + "." + e.ToString(), e.InnerException + "");
            }
        }

        private void insertarTraza()
        {
            try
            {
                string query = "";
                if (idEstado == 0)
                {
                    query = @"INSERT INTO Notificacion 
                                (Descripcion, idUsuarioReceptor, idUsuarioEmisor, FechaCreacion, Tipo, idFlujo, idFicha) 
                                VALUES 
                                ('" + this.Descripcion + "', '" +
                                    this.idUsuarioReceptor + "','" + this.idUsuarioEmisor + "', GETDATE(), '" + this.Tipo + "','" + idFlujo + "','" + idFicha + "')";
                }
                else
                {
                    query = @"INSERT INTO Notificacion 
                                (Descripcion, idUsuarioReceptor, idUsuarioEmisor, FechaCreacion, Tipo, idFlujo, idFicha, idEstado) 
                                VALUES 
                                ('" + this.Descripcion + "', '" +
                                    this.idUsuarioReceptor + "','" + this.idUsuarioEmisor + "', GETDATE(), '" + this.Tipo + "','" + idFlujo + "','" + idFicha + "','" + idEstado + "')";
                }


                

                DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("insertarTraza", "Notificacion", -1, e.ToString(), e.InnerException + "");
            }
        }

    }
}