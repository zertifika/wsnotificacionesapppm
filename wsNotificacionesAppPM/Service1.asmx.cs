﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using workFlow.classes;
using System.Web.Script.Serialization;

namespace wsNotificacionesAppPM
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service1 : System.Web.Services.WebService
    {
        private string idFlujo = "29";
        private string idUsuarioCompartido = "4393";

        private class siglas
        {
            public string entregadoADom = "FIR";
            public string dirIncorrecta = "DOE";
            public string ausenteReparto = "ABS";
            public string Desconocido = "DES";
            public string Fallecido = "DIF";
            public string Rehusado = "REF";
        }

        wsNotificacionesAppPM.wf.WorkFlow serv = new wf.WorkFlow();
        wsNotificacionesAppPM.api.Api apiWS = new api.Api();
        int idFormularioAusente = 138;
        int idFormularioFirma = 45;



        /// <summary>
        /// Usamos este método después de firmar el documento para indicar quien lo ha firmado.
        /// </summary>
        /// <param name="tokenUsuario"></param>
        /// <param name="idNotificacion"></param>
        /// <param name="nombreFirmante"></param>
        /// <param name="dniFirmante"></param>
        /// <returns></returns>
        [WebMethod]
        public string setFirmanteNotificacion(string tokenUsuario, string idNotificacion, string nombreFirmante,
            string dniFirmante, string enCalidadDe)
        {
            int idUsuario = -1;
            string query = string.Empty;
            try
            {
                if (Utilities.comprobarValidezUsuario(tokenUsuario) != 1) return "-1";

                idUsuario = Convert.ToInt32(tokenUsuario.Split('I')[0]);

                query = "SELECT TOP 1 idFicha FROM " + BasicoFlujo.getNombreTabla(idFlujo) +
                        " WHERE idNotificacion = '" + idNotificacion + "'";

                string idFicha = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow);

                if (!string.IsNullOrEmpty(idFicha))
                {
                    string codigoVerificacion = DatabaseConnection.executeScalarString(
                            "SELECT TOP 1 idGuid FROM DocsCompartidos WHERE idDoc IN " +
                            "(SELECT TOP 1 idDocumento FROM Documentos WHERE idFlujo = " + idFlujo +
                            " AND idFicha = " + idFicha + ")", CommandType.Text, ConnectionString.WorkFlow);

                    query = "UPDATE " + BasicoFlujo.getNombreTabla(idFlujo) + " SET NOMBRE_FIRMANTE = '" + nombreFirmante + "', " +
                        " DNI_FIRMANTE = '" + dniFirmante + "', EN_CALIDAD_DE = '" +
                        enCalidadDe + "', CODIGO_VERIFICACION = '" + codigoVerificacion + "'  WHERE idFicha = " + idFicha;

                    int resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);

                    string res = serv.actualizarFormularioWeb(tokenUsuario, idFlujo, idFicha, idFormularioFirma + "");

                    return resp + "";
                }

                return "-1";

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("setFirmanteNotificacion", "wsNotificaciones", -1, tokenUsuario + "." + idNotificacion +
                   " . " + nombreFirmante + " . " + dniFirmante, e.ToString());
                return "-1";
            }
        }

        [WebMethod]
        public string obtenerEstadoNotificacion(string usuario, string clave, string idNotificacion)
        {
            string query = string.Empty;
            string resultado = string.Empty;
            try
            {
                if (usuario.Equals("recaudacion") && clave.Equals("rrtt"))
                {
                    query = "select top 1 * from " + BasicoFlujo.getNombreTabla(idFlujo) + " where CB = '" + idNotificacion + "'";

                    DataTable dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                    if (dt.Rows.Count > 0)
                    {
                        /* 
                         *  estado: "En proceso/Finalizado"
                         *  resultado fecha y hora intento
                         * 
                         * */

                        using (SqlConnection con = new SqlConnection("Server=tcp:192.168.1.95;Database=edocNotificaciones;Uid=edoc;Pwd=edoc;Encrypt=True;Connection Timeout=3000;TrustServerCertificate=True"))
                        {
                            dt = DatabaseConnection.executeNonQueryDT(@"select top 1 idNotificacion, Nombre, Calle, Numero, KM, Escalera, Esc2, 
                                                                Piso, Puerta, CP, Localidad, Provincia, Res1, Fecha1, Hora1, Res2, Fecha2, Hora2 
                                                                from NOTIFICACIONES where CB = '" + idNotificacion + "'", CommandType.Text, con.ConnectionString);

                            if (dt.Rows.Count > 0)
                            {
                                string Res1 = dt.Rows[0]["Res1"] + "";


                                if (Res1 != "ABS")
                                {
                                    string Fecha1 = dt.Rows[0]["Fecha1"] + "";

                                    if (Fecha1.Contains(' ')) Fecha1 = Fecha1.Split(' ')[0];

                                    string Hora1 = dt.Rows[0]["Hora1"] + "";


                                    resultado += @"{ ""Estado"": ""Finalizado"", ""Fecha"": """ + Fecha1 + @""", ""Hora"": """ + Hora1 + @""", ""Motivo"": """ + Res1 + @""", ""Intento"": 1 }";
                                }
                                else
                                {
                                    string Res2 = string.Empty;
                                    string estado = string.Empty;
                                    string Fecha2 = string.Empty;
                                    string Hora2 = string.Empty;



                                    if (!Convert.IsDBNull(dt.Rows[0]["Res2"]))
                                    {
                                        Res2 = dt.Rows[0]["Res2"] + "";
                                        Fecha2 = dt.Rows[0]["Fecha2"] + "";
                                        Hora2 = dt.Rows[0]["Hora2"] + "";

                                        if (Fecha2.Contains(' ')) Fecha2 = Fecha2.Split(' ')[0];

                                    }

                                    if (string.IsNullOrEmpty(Res2))
                                    {
                                        resultado += @"{ ""Estado"": ""En proceso"", ""Intento"": 2 }";
                                    }
                                    else
                                    {
                                        resultado += @"{ ""Estado"": ""Finalizado"", ""Fecha"": """ + Fecha2 + @""", ""Hora"": """ + Hora2 + @""", ""Motivo"": """ + Res2 + @""", ""Intento"": 2 }";
                                    }



                                }

                            }
                            else
                            {
                                resultado += @"{ ""Estado"": ""En proceso"", ""Intento"": 1 }";
                            }

                        }



                    }
                    else
                    {
                        resultado += @"{ ""Estado"": ""Pendiente de procesar"" }";
                    }
                }
                return resultado;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("obtenerImagenNotificacion", "wsNotificaciones", -1, clave + "." + idNotificacion, e.ToString());
                return "-1";
            }

        }

        [WebMethod]
        public int iniciarRutaNotificador(string usuario, string clave, string latitud, string longitud)
        {
            // 2 ya se ha logeado ese dia
            // 1 primera vez que se logea, todo bien
            // -1
            string query = string.Empty;
            string idFormulario = "-1";
            try
            {
                string tokenUsuario = serv.getTokenUsuario(usuario, clave,"");
                if (Utilities.comprobarValidezUsuario(tokenUsuario) == 1)
                {
                    // Miramos si ya hay un flujo iniciado ese día, por este usuario

                    int idUsuario = Convert.ToInt32(tokenUsuario.Split('I')[0]);
                    if (idUsuario > 0)
                    {
                        string descripcionFlujo = DateTime.Now.Day + " " + DateTime.Now.Month + " " + DateTime.Now.Year + " - " + idUsuario;

                        query = "SELECT TOP 1 idFlujo FROM Descripcion WHERE idFlujo = 32 and Descripcion = '" + descripcionFlujo + "'";

                        DataTable dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                        if (dt.Rows.Count > 0) return 2;

                        string campos = getCamposXMLInicioRuta(idUsuario, latitud, longitud);

                        string res = serv.iniciarFlujoWeb(tokenUsuario, "32", descripcionFlujo, campos);

                        string idFicha = res.Split('#')[0];
                        // string asign = serv.asignarTodosLosPasos(tokenUsuario, tokenUsuario, idFlujo, idFicha);

                        string idDoc = serv.subirFicheroFormularioWeb(tokenUsuario, "32", idFicha, idFormulario + "");

                        return 1;
                    }
                }
                return -1;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("iniciarRutaNotificador", "wsNotificaciones", -1, usuario + "." + latitud + "." + longitud, e.ToString());
                return -1;
            }
        }

        [WebMethod]
        public string obtenerImagenNotificacion(string usuario, string clave, string idNotificacion)
        {
            string query = string.Empty;
            int idDocumento = -1;
            try
            {
                if (usuario.Equals("recaudacion") && clave.Equals("rrtt"))
                {
                    query = "select top 1 idFicha from " + BasicoFlujo.getNombreTabla(idFlujo) + " where CB = '" + idNotificacion + "'";

                    string idFicha = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow);

                    if (!string.IsNullOrEmpty(idFicha))
                    {

                        query = "select top 1 idDocumento from Documentos where idflujo = " + idFlujo + " and idficha = " + idFicha;

                        string idDoc = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow);

                        string rutaFichero = BasicoFlujo.getRutaDoc(idDoc);
                        if (File.Exists(rutaFichero))
                        {
                            byte[] bFichero = File.ReadAllBytes(rutaFichero);
                            string sFichero = Convert.ToBase64String(bFichero);
                            return sFichero;
                        }
                    }
                }
                return "-1";
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("obtenerImagenNotificacion", "wsNotificaciones", -1, clave + "." + idNotificacion, e.ToString());
                return "-1";
            }

        }

        //[WebMethod]
        //public string enviarMensaje(string tokenUsuario, string idNotificacion, string mensaje)
        //{
        //    string query = string.Empty;
        //    try
        //    {
        //        int idUsuario = Convert.ToInt32(tokenUsuario.Split('I')[0]);

        //        query = "select top 1 idFicha from f00000029 where idNotificacion = '" + idNotificacion + "'";

        //        int idFicha = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);
        //        if (idFicha > 0)
        //        {
        //            wf.WorkFlow wff = new wf.WorkFlow();
        //            string resp = wff.enviarMensajeInterno(idFlujo, idFicha + "", idUsuario + "", mensaje);
        //        }
        //        else
        //        {
        //            return "-1";
        //        }

        //        return "1";
        //    }
        //    catch (Exception e)
        //    {
        //        Trazas t = new Trazas("enviarMensaje", "wsNotificaciones", -1, tokenUsuario + "." + idNotificacion, e.ToString());
        //        return "-1";
        //    }
        //}

        [WebMethod]
        public string getTime()
        {
            return DateTime.Now.ToString();
        }

        [WebMethod]
        public string firmarDocumentoDesdeWeb(string tokenUsuario, string idNotificacion)
        {
            string resp = "-1";
            try
            {

                wf.WorkFlow w = new wf.WorkFlow();

                if (!string.IsNullOrEmpty(idNotificacion))
                {

                    string query = "select top 1 idDocumento from sicer where idNotificacion = " + idNotificacion;

                    string idDoc = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow);
                    if (!string.IsNullOrEmpty(idDoc))
                    {
                        resp = w.FirmarDocumentoDesdeWeb(tokenUsuario, idDoc, "PDF", 6, false, 0, 0, false);
                    }
                }

                return resp;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("firmarDocumentoDesdeWeb", "wsNotificacionesPM", -1, tokenUsuario + "." + idNotificacion +
                    " . ", e.ToString());
                return "-1";
            }
        }

        /// <summary>
        /// indicar el estado de la notificacion
        /// </summary>
        /// <param name="tokenUsuario"></param>
        /// <param name="idNotificacion"></param>
        /// <param name="estadoNotificacion"></param>
        /// <param name="numIntento"></param>
        /// <param name="Fecha"></param>
        /// <param name="Hora"></param>
        /// <param name="Longitud"></param>
        /// <param name="Latitud"></param>
        /// <returns> 1 o -1 dependiendo de si todo ok</returns>
        [WebMethod]
        public string setEstadoNotificacion(string tokenUsuario, string idNotificacion,
            string estadoNotificacion, int numIntento, string Fecha, string Hora, string Longitud, string Latitud, string Observaciones = "", string iniciar = "")
        {
            string idFicha = string.Empty;
            string query = string.Empty;
            string idEstado = string.Empty;
            string respuesta = string.Empty;
            int idUsuario = -1;
            string respServ = string.Empty;
            bool hayQueIniciar = string.IsNullOrEmpty(iniciar);
            siglas sig = new siglas();
            try
            {

                // si el usuario es correcto y tal y tal
                if (numIntento != 1 && numIntento != 2) return "-1";

                if (Utilities.comprobarValidezUsuario(tokenUsuario) != 1) return "-1";

                idUsuario = Convert.ToInt32(tokenUsuario.Split('I')[0]);

                query = "SELECT TOP 1 Nombre, [Password] FROM Usuarios WHERE idUsuario = " + idUsuario;
                DataTable dtUsuario = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                if (dtUsuario.Rows.Count == 0) return "-1";
                string nombreUsuario = dtUsuario.Rows[0]["Nombre"] + "";
                string claveUsuario = dtUsuario.Rows[0]["Password"] + "";

                string Posicion = Latitud + ":" + Longitud;
                string Posicion2 = Latitud + "," + Longitud;
                string xmlCampos = getCamposXMLNotificacion(idNotificacion, numIntento, idUsuario, Posicion, Fecha, Hora, estadoNotificacion);
                DateTime date = DateTime.Parse(Fecha);

                query = "select top 1 cb from sicer where idNotificacion = " + idNotificacion;
                string CB = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.Notificaciones);
                
                string idDoc = apiWS.rellenarDocumentoFormulario(nombreUsuario, claveUsuario, idFormularioAusente.ToString(),
                    CB + "-" + numIntento , xmlCampos);

                int res = compartirDocumento(idDoc, idUsuario.ToString());

                res = asignarTipo(estadoNotificacion, idUsuario.ToString(), idDoc, Posicion2);

                if (!string.IsNullOrEmpty(idDoc) && !idDoc.Equals("-1"))
                {
                    //wf.WorkFlow w = new wf.WorkFlow();
                    //w.FirmarDocumentoDesdeWeb(tokenUsuario, idDoc, "PDF", 6, false, 0, 0, false);
                    
                    // Hemos guardado el documento y si se ha guardado correctamente se actualizará en la base de datos.
                    query = "UPDATE sicer SET RES" + numIntento + " = '" + estadoNotificacion + "', " +
                            " FECHA" + numIntento + " = '" + date + "', " +
                            " HORA" + numIntento + " = '" + Hora + "', " +
                            " USUARIO" + numIntento + " = '" + nombreUsuario + "', " +
                            " POS" + numIntento + " = '" + Posicion + "', " +
                            " OBSERVACIONES" + numIntento + " = '" + Observaciones + "', " +
                            " idUsuario" + numIntento + " = '" + idUsuario + "', " +
                            " numeroIntento = '" + numIntento + "', " +
                            " idDocumento" + numIntento + " = '" + idDoc + "' " +
                            " WHERE idNotificacion = " + idNotificacion;


                    res = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.Notificaciones);
                }

                if(numIntento > 1)
                {
                    res = eliminarTipoPrimerIntento(idNotificacion);
                }
                if (res != -1 && !string.IsNullOrEmpty(idDoc) && !idDoc.Equals("-1")) 
                    return "1";
                else
                    return "-1";
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("setEstadoNotificaciones", "wsNotificaciones", -1, tokenUsuario + "." + idNotificacion +
                    " . " + numIntento + " . " + estadoNotificacion, e.ToString());
                return "-1";
            }
        }

        private int compartirDocumento(string idDoc, string idUsuario)
        {
            int res = 0;
            try
            {
                string idEmpresa = DatabaseConnection.executeScalarString("select idEmpresa from usuarioempresa where idUsuario = " + idUsuarioCompartido, CommandType.Text, ConnectionString.WorkFlow);
                string query = "insert into DocumentosUsuarios (idDocumento, idUsuario, idUsuarioComparte, FechaInclusion, idempresausuariocomparte) " +
                    "values (" + idDoc + "," + idUsuarioCompartido + "," + idUsuario + ",'" + DateTime.Today + "'," + idEmpresa + ")";

                res = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);

                return res;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("compartirDocumento", "wsNotificacionesAppPm", -1, e.Message, e.ToString());
                return -1;
            }
        }

        private int asignarTipo (string resultado, string idUsuario, string idDoc, string posicion)
        {
            int res = 0;
            try
            {
                string tipo = devolverTipoNotificacion(resultado);
                string tabla = "c00000000";
                tabla = tabla.Substring(0, tabla.Length - tipo.Length) + tipo;

                posicion = posicion.Replace(":", ",");

                string query = "insert into " + tabla + " (posicion) values ('" + posicion + "'); select SCOPE_IDENTITY();";
                string idMetadato = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow).ToString();
                
                query = "insert into tiposdocumento (idDoc, idTipoDocumento, idMetadato, indexado, activo) values (" + idDoc + "," + tipo + "," + idMetadato + ", 0, 1);";
                res = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);
                return res;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("asignarTipo", "wsNotificacionesAppPm", -1, e.Message, e.ToString());
                return -1;
            }
        }

        private string devolverTipoNotificacion(string resultado)
        {
            switch (resultado.ToUpper())
            {
                case "ABS":
                    return "258";
                case "FIR":
                    return "259";
                case "DES":
                    return "260";
                case "DOE":
                    return "261";
                case "DIF":
                    return "262";
                case "REH":
                    return "263";
                // NHC como último caso
                default:
                    return "264";
            }
        }

        private int eliminarTipoPrimerIntento(string idNotificacion)
        {
            int res = -1;
            try
            {
                string query = "selEct idDocumento1 from sicer where idNotificacion = " + idNotificacion;
                string idDoc = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.Notificaciones);

                if (!string.IsNullOrEmpty(idDoc))
                {
                    query = "update tiposdocumento set activo = 0 where idDoc = " + idDoc;
                    res = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);
                    query = "update documentos set activo = 0 where idDocumento = " + idDoc;
                    res = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);
                    return res;
                }
                else
                    return -1;
            }
            catch (Exception e)
           {
                Trazas t = new Trazas("eliminarTipoPrimerIntento", "wsNotificacionesAppPm", -1, e.Message, e.ToString());
                return -1;
            }
        }

        [WebMethod]
        public string generaIncidencia(string tokenUsuario, string idNotificacion, string asunto, string cuerpo)
        {
            string query = string.Empty;
            string idFlujo = "30";
            // -1 Default
            string idFormulario = "-1";
            try
            {
                if (Utilities.comprobarValidezUsuario(tokenUsuario) != 1) return "-1";

                string xmlCampos = getCamposXMLIncidencia(idNotificacion, asunto, cuerpo);
                string res = serv.iniciarFlujoWeb(tokenUsuario, idFlujo, idNotificacion, xmlCampos);
                string idFicha = res.Split('#')[0];
                // string asign = serv.asignarTodosLosPasos(tokenUsuario, tokenUsuario, idFlujo, idFicha);

                string idDoc = serv.subirFicheroFormularioWeb(tokenUsuario, idFlujo, idFicha, idFormulario + "");
                if (Convert.ToInt32(idDoc.Split('#')[0]) > 0)
                {
                    return "1";
                }
                return "-1";
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("generaIncidencia", "wsNotificaciones", -1, tokenUsuario + "." + idNotificacion +
                    " . " + asunto + " . " + cuerpo, e.ToString());
                return "-1";
            }
        }

        private string getCamposXMLIncidencia(string idNotificacion, string asunto, string cuerpo)
        {
            string query = string.Empty;
            StringBuilder resp = new StringBuilder();
            try
            {

                resp.Append("<campos><campo>");
                resp.Append("<idNotificacion>" + idNotificacion + "</idNotificacion>");
                resp.Append("<ASUNTO>" + asunto + "</ASUNTO>");
                resp.Append("<CUERPO>" + cuerpo + "</CUERPO>");
                resp.Append("</campo></campos>");


                return resp.ToString();
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getCamposXMLNotificacion", "wsNotificaciones", -1, idNotificacion, e.ToString());
                return "-1";
            }
        }

        private string getCamposXMLInicioRuta(int idUsuario, string latitud, string longitud)
        {
            string query = string.Empty;
            StringBuilder resp = new StringBuilder();
            try
            {

                resp.Append("<campos><campo>");
                resp.Append("<latitud>" + latitud + "</latitud>");
                resp.Append("<longitud>" + longitud + "</longitud>");
                resp.Append("</campo></campos>");


                return resp.ToString();
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getCamposXMLInicioRuta", "wsNotificaciones", idUsuario, latitud + " " + longitud, e.ToString());
                return "-1";
            }
        }

        /// <summary>
        ///  Genera el documento a firmar antes de hacer el set estado
        /// </summary>
        /// <param name="tokenUsuario"></param>
        /// <param name="idNotificacion"></param>
        /// <param name="estadoNotificacion"></param>
        /// <param name="numIntento"></param>
        /// <param name="Fecha"></param>
        /// <param name="Hora"></param>
        /// <param name="Longitud"></param>
        /// <param name="Latitud"></param>
        /// <returns></returns>
        [WebMethod]
        public string generaDocumento(string tokenUsuario, string idNotificacion,
            string estadoNotificacion, int numIntento, string Fecha, string Hora, string Longitud, string Latitud, string Observaciones = "")
        {
            int idUsuario = -1;
            string idFicha = "";
            string query = string.Empty;
            string res = string.Empty;
            string idDoc = string.Empty;
            try
            {



                // si el usuario es correcto y tal y tal
                if (numIntento != 1 && numIntento != 2) return "-1";

                if (Utilities.comprobarValidezUsuario(tokenUsuario) != 1) return "-1";

                // Si ya hay un flujo con este idnotificacion devolvemos menos uno también
                query = "select top 1 Descripcion from descripcion where descripcion = '" + idNotificacion + "'";

                //if ((DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow).Rows.Count > 0) && (numIntento == 1))
                //    // ya hay uno con ese id
                //    return "-1";

                idUsuario = Convert.ToInt32(tokenUsuario.Split('I')[0]);

                string Posicion = Longitud + ":" + Latitud;

                query = "SELECT TOP 1 idFicha FROM " + BasicoFlujo.getNombreTabla(idFlujo) +
                        " WHERE idNotificacion = '" + idNotificacion + "'";

                idFicha = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow);

                query = "SELECT TOP 1 Nombre, [Password] FROM Usuarios WHERE idUsuario = " + idUsuario;
                DataTable dtUsuario = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                if (dtUsuario.Rows.Count == 0) return "-1";
                string nombreUsuario = dtUsuario.Rows[0]["Nombre"] + "";

                if (numIntento == 1 && string.IsNullOrEmpty(idFicha))
                {
                    // Al ser el primer intento y ya haber actualizado creamos un flujo nuevo
                    string xmlCampos = getCamposXMLNotificacion(idNotificacion, numIntento, idUsuario, Posicion, Fecha, Hora, estadoNotificacion);
                    res = serv.iniciarFlujoWeb(tokenUsuario, idFlujo, idNotificacion, xmlCampos);
                    idFicha = res.Split('#')[0];
                    // string asign = serv.asignarTodosLosPasos(tokenUsuario, tokenUsuario, idFlujo, idFicha);

                    idDoc = serv.subirFicheroFormularioWeb(tokenUsuario, idFlujo, idFicha, idFormularioFirma + "");
                    if (idDoc.Contains('#'))
                    {
                        idDoc = idDoc.Split('#')[0];
                        res = BasicoFlujo.cambiarNombreADocumentoPrincipal(idDoc, idNotificacion + "-" + numIntento + ".pdf", idUsuario);
                    }
                    else
                    {
                        new Trazas("generaDocumento", "Service", idUsuario, "No sube el formulario", idDoc);
                    }
                }
                else
                {
                    // ACTUALIZAMOS
                    query = "UPDATE " + BasicoFlujo.getNombreTabla(idFlujo) + " SET INTENTO = " + numIntento + ", FECHA" + numIntento + " = '" + Fecha +
                        "', HORA" + numIntento + " = '" + Hora + "', RESULTADO" + numIntento + " = '" + estadoNotificacion + "' WHERE idNotificacion =" + idNotificacion;
                    int resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);

                    res = serv.subirFicheroAdjuntoFormularioWeb(tokenUsuario, idFlujo, idFicha, idFormularioFirma + "");

                    if (res == "1")
                    {
                        // obtenemos el iddoc y el iddocadjunto
                        query = @"select top 1 doc.idDocumento, da.idDocAdjunto from Documentos doc 
                                        inner join DocumentosAdjuntos da on 
                                        da.idDocumento = doc.idDocumento
                                        where doc.idflujo = " + idFlujo + " and doc.idficha = " + idFicha;

                        DataTable dtDocs = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                        if (dtDocs.Rows.Count > 0)
                        {
                            idDoc = dtDocs.Rows[0]["idDocumento"] + "";
                            int idDocAdjunto = Convert.ToInt32(dtDocs.Rows[0]["idDocAdjunto"]);

                            res = serv.marcarAdjuntoComoPrincipalWeb(tokenUsuario, idDocAdjunto + "", idDoc + "");
                            // obtengo el principal y le cambio el nombre
                            idDoc = BasicoFlujo.getIdDocumento(idFlujo, idFicha);
                            res = BasicoFlujo.cambiarNombreADocumentoPrincipal(idDoc, idNotificacion + "-" + numIntento + ".pdf", idUsuario);
                            if (res != "1")
                            {
                                new Trazas("marcarAdjuntoComoPrincipalWeb", "generaDocumento", idUsuario, "No ha subido el fichero adjunto", tokenUsuario + " . " + idDocAdjunto + " . " + idDoc);
                                return "-1";
                            }
                            else
                            {
                                // Devolvemos el idDoc
                                res = idDoc + "";

                            }


                        }
                    }
                    else
                    {
                        new Trazas("subirficheroadjuntoformulario", "generaDocumento", idUsuario, "No ha subido el fichero adjunto", "");
                    }

                }

                string rresp = setEstadoNotificacion(tokenUsuario, idNotificacion, estadoNotificacion, numIntento, Fecha, Hora, Longitud, Latitud, Observaciones, "false");


                // Devolvemos el iddoc
                if (res == "-1")
                {
                    return "-1";
                }
                else
                {
                    //string idDoc = res.Split('#')[0];
                    return "{ idDoc: " + idDoc + " }";
                }
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("generaDocumento", "wsNotificaciones", idUsuario, tokenUsuario + "." + idNotificacion +
                    " . " + numIntento + " . " + estadoNotificacion, e.ToString());
                return "-1";
            }
        }

        /// <summary>
        /// Devuelve el idDoc del documento en formato JSON.
        /// </summary>
        /// <param name="tokenUsuario"></param>
        /// <param name="idNotificacion"></param>
        /// <returns></returns>
        [WebMethod]
        public string getDocumentoNotificacion(string tokenUsuario, string idNotificacion)
        {
            string query = string.Empty;
            try
            {
                query = "SELECT TOP 1 idFicha FROM " + BasicoFlujo.getNombreTabla(idFlujo) +
                        " WHERE idNotificacion = '" + idNotificacion + "'";

                string idFicha = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow);
                if (!string.IsNullOrEmpty(idFicha))
                {
                    query = @"select top 1 idDocumento from Documentos where idflujo = " + idFlujo + " and idficha = " + idFicha;
                    string idDoc = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow);
                    return "{ idDoc: " + idDoc + " }";
                }
                else
                {
                    return "-1";
                }
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getDocumentoNotificacion", "wsNotificaciones", -1, tokenUsuario, e.ToString());
                return "-1";
            }
        }

        [WebMethod]
        public string getInfoFestividades(string tokenUsuario)
        {
            string query = string.Empty;
            string respuesta = string.Empty;
            try
            {

                respuesta = "25/12/2015^26/12/2015^01/01/2016^06/01/2016^20/01/2016^01/03/2016^24/03/2016^25/03/2016^28/03/2016^02/04/2016^03/04/2016^06/04/2016^01/05/2016^15/08/2016^12/10/2016^02/11/2016^07/12/2016^08/12/2016^25/12/2016^26/12/2016";

                return respuesta;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getInfoFestividades", "wsNotificaciones", -1, tokenUsuario, e.ToString());
                return "-1";
            }
        }

        /// <summary>
        /// Devuelve la información de la notificación cogiendola de la base de datos de notificaciones
        /// </summary>
        /// <param name="tokenUsuario"></param>
        /// <param name="CB"></param>
        /// <returns></returns>
        [WebMethod]
        public string getInfoNotificacion(string tokenUsuario, string CB)
        {

            try
            {
                CB = Utilities.tratarParam(CB);
                tokenUsuario = Utilities.tratarParam(tokenUsuario);

                DataTable dt = new DataTable();

                dt = DatabaseConnection.executeNonQueryDT(@"select top 1 idNotificacion, CB, Nombre, DIRECCION, CP, POBLACION, CODREMESA, Res1, Fecha1, Hora1, Res2, Fecha2, Hora2, numeroIntento
                                                            from sicer where CB = '" + CB + "'", CommandType.Text, ConnectionString.Notificaciones);

                dt.Columns.Add("Iniciado");
                dt.Columns.Add("NumFirmas");

                if (dt.Rows.Count > 0)
                {
                    int idNotificacion = Convert.ToInt32(dt.Rows[0]["idNotificacion"]);
                    int numeroIntento = Convert.ToInt32(dt.Rows[0]["numeroIntento"]);
                    if(numeroIntento > 0)
                    {
                        // ya hay uno con ese id
                        dt.Rows[0]["Iniciado"] = 1;
                        string query = @"select top 1 numFirmasBio from documentos where docOriginal = '" + idNotificacion + "-1.pdf'";

                        string numFirmasBio = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow);

                        dt.Rows[0]["NumFirmas"] = numFirmasBio;
                    }
                    else
                    {
                        dt.Rows[0]["Iniciado"] = 0;
                        dt.Rows[0]["NumFirmas"] = 0;
                    }
                }
                else
                {
                    dt.Rows[0]["Iniciado"] = 0;
                    dt.Rows[0]["NumFirmas"] = 0;
                }


                return BasicoFlujo.DataTableToJSON(dt);
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getInfoNotificacion", "wsNotificaciones", -1, tokenUsuario + ".", e.ToString());
                return "-1";
            }
        }

        #region Métodos Notificaciones v2.0

        [WebMethod]
        public string crearRemesa(string tokenUsuario, string descripcion, string notificaciones)
        {
            string respuesta = string.Empty;
            try
            {
                descripcion = Utilities.tratarParam(descripcion);
                tokenUsuario = Utilities.tratarParam(tokenUsuario);
                if(Utilities.comprobarValidezUsuario(tokenUsuario) == 1)
                {
                   string idRemesa = DatabaseConnection.executeScalarString(
                        "insert into remesas (descripcion, fechacreacion) values ('" + descripcion + "', GETDATE());SELECT SCOPE_IDENTITY()  ;", 
                        CommandType.Text, ConnectionString.Notificaciones2);

                    bool hayErrores = false;

                    string[] not = notificaciones.Split(' ');
                    for(int i = 0; i < not.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(not[i]))
                        {
                            int  resp = DatabaseConnection.executeNonQueryInt(
                           "insert into notificaciones (CB, idRemesa) values ('" + not[i] + "', " + idRemesa + ");SELECT SCOPE_IDENTITY();",
                           CommandType.Text, ConnectionString.Notificaciones2);

                            if (resp != 1)
                            {
                                hayErrores = true;
                                respuesta += "|" + not[i];
                            }

                        }
                    }

                    if (hayErrores)
                    {
                        return "0" + respuesta;
                    }    

                    return "1";
                }

                
                return "-1";
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("crearRemesa", "wsNotificaciones", -1, tokenUsuario + ".", e.ToString());
                return "-1";
            }
        }

        [WebMethod]
        public DataTable getRemesas(string tokenUsuario)
        {
            DataTable dt = new DataTable();
            try
            {
                tokenUsuario = Utilities.tratarParam(tokenUsuario);

                string query = "select id, descripcion, fechacreacion from remesas order by fechacreacion desc";
                dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.Notificaciones2);
                dt.TableName = "remesas";
                return dt;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getRemesas", "wsNotificaciones", -1, tokenUsuario + ".", e.ToString());
                return null;
            }
        }

        [WebMethod]
        public DataTable getNotificadores(string tokenUsuario)
        {
            DataTable dt = new DataTable();
            try
            {
                tokenUsuario = Utilities.tratarParam(tokenUsuario);

                // Sacamos los CBs de esa remesa
                string query = @"select idusuario, nombre, apellidos, email from Usuarios
                                      where exists(
                                      select * from UsuarioEmpresa
                                      where usuarios.idusuario = UsuarioEmpresa.idUsuario and idempresa = 8)";



                dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                dt.TableName = "notificaciones";

                return dt;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getRemesas", "wsNotificaciones", -1, tokenUsuario + ".", e.ToString());
                return null;
            }
        }


        [WebMethod]
        public DataTable getNotificaciones(string tokenUsuario, int idRemesa)
        {
            DataTable dt = new DataTable();
            try
            {
                tokenUsuario = Utilities.tratarParam(tokenUsuario);

                // Sacamos los CBs de esa remesa
                string query = "select CB from notificaciones where idRemesa = " + idRemesa;
                dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.Notificaciones2);

                // Obtenemos las notificaciones a partir de los CBs

                string s = " ( ";

                for(int i = 0; i < dt.Rows.Count; i++)
                {
                    s+="'" + dt.Rows[i][0] + "', ";
                }

                s = s.Substring(0, s.Length - 2);

                s += " ) ";

                query = "select * from notificaciones where CB in " + s;
                dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.edocNotificaciones);
                dt.TableName = "notificaciones";
                return dt;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getRemesas", "wsNotificaciones", -1, tokenUsuario + ".", e.ToString());
                return null;
            }
        }


        [WebMethod]
        public string asignarNotificaciones(string tokenUsuario, List<string> notificaciones, int idUsuario, int idRemesa)
        {
            try
            {
                // Limpiamos
                tokenUsuario = Utilities.tratarParam(tokenUsuario);
                for(int i=0; i<notificaciones.Count;i++)
                {
                    notificaciones[i] = Utilities.tratarParam(notificaciones[i]);
                }


                string cbs = string.Empty;

                cbs += "( "; 
                for(int i = 0; i < notificaciones.Count; i++)
                {
                    cbs += "'" + notificaciones[i] + "', ";
                }
                cbs = cbs.Substring(0, cbs.Length - 2);
                cbs += " )";

                string query = "UPDATE notificaciones SET idUsuario = " + idUsuario + " WHERE CB in " + cbs;
                int resp = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.Notificaciones2);

                return "1";
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("addNotificaciones", "wsNotificaciones", -1, tokenUsuario + ".", e.ToString());
                return "-1";
            }
        }


        /*
           * 
           * 
           * [{
"idNotificacion":"idNotificacion",
              "cb": "cb",
              "nombre": "nombre",
              "direccion": "direccion",
“cp”:”cp”,
"localidad":"localidad",
"remesa":"remesa",
              "res1": "res1",
"fecha1":"fecha1",
"hora1":"hora1",
"res2": "res1",
"fecha2":"fecha1",
"hora2":"hora1",
"pos1":"pos1",
"pos2":"pos2",
"usuario1": "usuario1",
"usuario2": "usuario2",
"observaciones1":"observaciones1",
"observaciones2":"observaciones2",

}, {
              "idNotificacion":"idNotificacion",
              "cb": "cb",
              "nombre": "nombre",
              "direccion": "direccion",
“cp”:”cp”,
"localidad":"localidad",
"remesa":"remesa",
              "res1": "res1",
"fecha1":"fecha1",
"hora1":"hora1",
"res2": "res1",
"fecha2":"fecha1",
"hora2":"hora1",
"pos1":"pos1",
"pos2":"pos2",
"usuario1": "usuario1",
"usuario2": "usuario2",
"observaciones1":"observaciones1",
"observaciones2":"observaciones2",
}]

           * 
           * */
        [WebMethod]
        public string getNotificacionesOffline(string tokenUsuario)
        {
            StringBuilder respuesta = new StringBuilder();
            string resp = string.Empty;
            try
            {
                // Hacemos la query para obtener todas las notificaciones que se han asignado a ese usuario

                tokenUsuario = Utilities.tratarParam(tokenUsuario);

                if (tokenUsuario.Contains('I'))
                {
                    int idUsuario = Convert.ToInt32(tokenUsuario.Split('I')[0]);

                    if (idUsuario > 0)
                    {
                        string query = string.Empty;

                        query = string.Format(@"select CB from notificaciones where idusuario = {0} and 
                                exists(select * from remesas where remesas.id = notificaciones.idremesa and remesas.estado = 1)", idUsuario);

                        DataTable dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.Notificaciones2);

                        if(dt.Rows.Count > 0)
                        {
                            // Obtenemos la información de todas estas notificaciones

                            string notificaciones = string.Empty;

                            for(int i = 0; i < dt.Rows.Count; i++)
                            {
                                notificaciones += "'" + dt.Rows[i]["CB"] + "',";
                            }

                            // Quito la coma
                            if (notificaciones.Contains(',')) notificaciones = notificaciones.Substring(0, notificaciones.Length - 1);

                            // Obtenemos la información de las notificaciones seleccionadas

                            query = @"select 
                                        idNotificacion, CB, NOMBRE, CALLE + ' ' + NUMERO + ' ' + PROVINCIA AS DIRECCION,
                                        CP, LOCALIDAD, REMESA, RES1, FECHA1, HORA1, RES2, FECHA2, HORA2, POS1, POS2, 
                                        USUARIO1, USUARIO2, OBSERVACIONES1, OBSERVACIONES2
                                        from notificaciones where cb in (" + notificaciones + ")";
                            dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.edocNotificaciones);


                            resp += "[";
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {

                                resp += @"{
	                            ""idNotificacion"": """ + getValue(dt.Rows[i]["idNotificacion"]) + @""",
                               ""cb"": """ + getValue(dt.Rows[i]["CB"]) + @""",
                                ""nombre"": """ + getValue(dt.Rows[i]["NOMBRE"]) + @""",
                                ""direccion"": """ + getValue(dt.Rows[i]["DIRECCION"]) + @""",
                                ""cp"": """ + getValue(dt.Rows[i]["CP"]) + @""",
                                ""localidad"": """ + getValue(dt.Rows[i]["LOCALIDAD"]) + @""",
                                ""remesa"": """ + getValue(dt.Rows[i]["REMESA"]) + @""",
                                ""res1"": """ + getValue(dt.Rows[i]["RES1"]) + @""",
                                ""fecha1"": """ + getValue(dt.Rows[i]["FECHA1"]) + @""",
                                ""hora1"": """ + getValue(dt.Rows[i]["HORA1"]) + @""",
                                ""res2"": """ + getValue(dt.Rows[i]["RES2"]) + @""",
                                ""fecha2"": """ + getValue(dt.Rows[i]["FECHA2"]) + @""",
                                ""hora2"": """ + getValue(dt.Rows[i]["HORA2"]) + @""",
                                ""pos1"": """ + getValue(dt.Rows[i]["POS1"]) + @""",
                                ""pos2"": """ + getValue(dt.Rows[i]["POS2"]) + @""",
                                ""usuario1"": """ + getValue(dt.Rows[i]["USUARIO1"]) + @""",
                                ""usuario2"": """ + getValue(dt.Rows[i]["USUARIO2"]) + @""",
                                ""observaciones1"": """ + getValue(dt.Rows[i]["OBSERVACIONES1"]) + @""",
                                ""observaciones2"": """ + getValue(dt.Rows[i]["OBSERVACIONES2"]) + @"""
                                },";


                             //   respuesta.AppendFormat(@"
                             //   {
	                            //'idNotificacion': '{0}',
                             //   'cb': '{1}',
                             //   'nombre': '{2}',
                             //   'direccion': '{3}',
                             //   'cp': '{4}',
                             //   'localidad': '{5}',
                             //   'remesa': '{6}',
                             //   'res1': '{7}',
                             //   'fecha1': '{8}',
                             //   'hora1': '{9}',
                             //   'res2': '{10}',
                             //   'fecha2': '{11}',
                             //   'hora2': '{12}',
                             //   'pos1': '{13}',
                             //   'pos2': '{14}',
                             //   'usuario1': '{15}',
                             //   'usuario2': '{16}',
                             //   'observaciones1': '{17}',
                             //   'observaciones2': '{18}'
                             //   },", , getValue(dt.Rows[i][""]), getValue(dt.Rows[i][""]), getValue(dt.Rows[i][""]), 
                             //   getValue(dt.Rows[i][""]),getValue(dt.Rows[i][""]), getValue(dt.Rows[i][""]), getValue(dt.Rows[i][""]), getValue(dt.Rows[i][""]), 
                             //   getValue(dt.Rows[i][""]), getValue(dt.Rows[i][""]), getValue(dt.Rows[i][""]), getValue(dt.Rows[i][""]), getValue(dt.Rows[i][""]), 
                             //   getValue(dt.Rows[i][""]),
                             //  getValue(dt.Rows[i][""]), getValue(dt.Rows[i][""]), getValue(dt.Rows[i][""]), getValue(dt.Rows[i][""]));

                            }

                            // Quitamos ultima coma
                            if (resp.Contains(',')) resp = resp.Substring(0, resp.Length - 1);
                            resp += "]";

                        }

                    }

                }


                return resp;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getNotificacionesOffline", "wsNotificaciones", -1, tokenUsuario + ".", e.ToString());
                return "-1";
            }

          
        }

        private string getValue(object v)
        {
            try
            {
                if (v != null && !Convert.IsDBNull(v) && !string.IsNullOrEmpty(v.ToString()))
                {
                    return v.ToString();
                }
                return "";
            }catch(Exception e)
            {
                return "";
            }
        }



        #endregion

        #region métodos que usa ATIB

        /// <summary>
        /// Devuelve el array de bytes de la notificación a partir de su idNotificacion y el numintento
        /// </summary>
        /// <param name="Usuario"></param>
        /// <param name="Clave"></param>
        /// <param name="CB"></param>
        /// <param name="numIntento"></param>
        /// <returns></returns>
        [WebMethod]
        public string getNotificacion(string Usuario, string Clave, string CB, int numIntento)
        {
            string respuesta = string.Empty;
            string query = string.Empty;
            int idNotificacion = -1;
            try
            {
                if (Usuario.Equals("recaudacion") && Clave.Equals("rrtt"))
                {
                    // Primero obtengo los datos actuales del proceso
                    query = @"SELECT TOP 1 idNotificacion, CB, NUMERO_CERTIFICADO, NOMBRE_FIRMANTE, 
                            DIRECCION, CP, POBLACION, INTENTO, FECHA, HORA, RESULTADO, DNI_FIRMANTE, 
                            UBICACION, NOMBRE_USUARIO, CODIGO_VERIFICACION, NOMBRE, EN_CALIDAD_DE
                            FROM " + BasicoFlujo.getNombreTabla(idFlujo) + " WHERE CB = '" + CB + "'";

                    DataTable dtNotificacion = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                    if (dtNotificacion.Rows.Count == 0)
                    {
                        respuesta = "-1";
                    }
                    else
                    {
                        idNotificacion = Convert.ToInt32(dtNotificacion.Rows[0]["idNotificacion"]);
                        if (idNotificacion > 0)
                        {
                            // Buscamos el documento del intento y la notificacion
                            // Ordeno por iddocumento por si acaso hubiera dos iguales coger el último
                            query = "SELECT TOP 1 idDocumento FROM Documentos WHERE docOriginal = '" +
                                idNotificacion + "-" + numIntento + ".pdf' ORDER BY idDocumento DESC";

                            string idDocumento = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow);

                            if (!string.IsNullOrEmpty(idDocumento))
                            {
                                string rutaFichero = BasicoFlujo.getRutaDoc(idDocumento);
                                if (File.Exists(rutaFichero))
                                {
                                    byte[] bFichero = File.ReadAllBytes(rutaFichero);
                                    string sFichero = Convert.ToBase64String(bFichero);
                                    return sFichero;
                                }
                                else
                                {
                                    respuesta = "-1";
                                }
                            }
                            else
                            {
                                // PODRÍA SER UN ADJUNTO, LO BUSCAMOS
                                query = "SELECT TOP 1 idDocAdjunto FROM DocumentosAdjuntos WHERE docOriginal = '" +
                                idNotificacion + "-" + numIntento + ".pdf' ORDER BY idDocAdjunto DESC";

                                idDocumento = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow);

                                if (!string.IsNullOrEmpty(idDocumento))
                                {
                                    string rutaFichero = BasicoFlujo.getRutaDocAdjunto(idDocumento, ".pdf");
                                    if (File.Exists(rutaFichero))
                                    {
                                        byte[] bFichero = File.ReadAllBytes(rutaFichero);
                                        string sFichero = Convert.ToBase64String(bFichero);
                                        return sFichero;
                                    }
                                    else
                                    {
                                        respuesta = "-1";
                                    }
                                }


                            }

                        }
                        else
                        {
                            respuesta = "-1";
                        }

                    }


                }
                else
                {
                    respuesta = "-1";
                }

                return respuesta;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getEstadoNotificacion", "wsNotificaciones", -1, Usuario + " . " + Clave + " . " + CB, e.ToString());
                return "-1";
            }
        }

        /// <summary>
        ///  Devuelve la información de la notificación a partir de la base de datos de notificaciones
        /// </summary>
        /// <param name="Usuario"></param>
        /// <param name="Clave"></param>
        /// <param name="CB"></param>
        /// <returns></returns>
        [WebMethod]
        public string getEstadoNotificacion(string Usuario, string Clave, string CB)
        {
            string respuesta = string.Empty;
            string query = string.Empty;
            wf.WorkFlow ws = new wf.WorkFlow();
            try
            {
                string token = ws.getTokenUsuario(Usuario, Clave,"");

                if (Utilities.comprobarValidezUsuario(token) != -1)
                {
                    DataTable dt = new DataTable();
                    query = "select top 1 idNotificacion, CB, Nombre, DIRECCION, CP, POBLACION, CODREMESA, Res1, Fecha1, Hora1, Res2, Fecha2, Hora2, numeroIntento from sicer where CB = '" + CB + "'";
                    dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.Notificaciones);

                    return BasicoFlujo.DataTableToJSON(dt);
                }
                else
                {
                    respuesta = "-1";
                }
                
                return respuesta;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getEstadoNotificacion", "wsNotificaciones", -1, Usuario + " . " + Clave + " . " + CB, e.ToString());
                return "-1";
            }
        }

        /// <summary>
        ///  Devuelve la información de las últimas 15 notificaciones
        /// </summary>
        /// <param name="Usuario"></param>
        /// <param name="Clave"></param>
        /// <param name="CB"></param>
        /// <returns></returns>
        [WebMethod]
        public string getHistoricoUsuario(string Usuario, string Clave)
        {
            string resultado = string.Empty;
            string CB = string.Empty;
            wf.WorkFlow ws = new wf.WorkFlow();
            try
            {
                string token = ws.getTokenUsuario(Usuario, Clave, "");

                if (Utilities.comprobarValidezUsuario(token) != -1)
                {
                    string idUsuario = token.Split('I')[0];
                    
                    DataTable dt = new DataTable();
                    string query = @"select top 15 idNotificacion, CB, Nombre, DIRECCION, CP, POBLACION, CODREMESA, Res1, Fecha1, Hora1, idUsuario1, Res2, Fecha2, Hora2, idUsuario2, numeroIntento 
                                    from sicer where (fecha1 >= '" + DateTime.Today + "' and fecha1 is not null and idUsuario1 = " + idUsuario + @") or 
                                                     (fecha2 >= '" + DateTime.Today + "' and fecha2 is not null and idUsuario2 = " + idUsuario + ")";
                    dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.Notificaciones);
                    
                    foreach (DataRow dr in dt.Rows)
                    {
                        CB = dr["CB"].ToString();
                        int numIntento = Convert.ToInt32(dr["numeroIntento"].ToString());


                        resultado += "|" + dr["Res"+numIntento] + "^" + dr["Fecha"+numIntento] + "^" + dr["Hora"+numIntento] + "^" + dr["CB"];
                    }
                }
                else
                {
                    resultado = "-1";
                }


                return resultado;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getEstadoNotificacion", "wsNotificaciones", -1, Usuario + " . " + Clave + " . " + CB, e.ToString());
                return "-1";
            }
        }

        #endregion


        #region métodos privados

        private string getCamposXMLNotificacion(string idNotificacion, int numIntento, int idUsuario, string ubicacion, string fecha, string hora, string resultado)
        {
            string query = string.Empty;
            StringBuilder resp = new StringBuilder();
            try
            {

                string nombreUsuario = DatabaseConnection.executeScalarString(
                                "SELECT Nombre FROM Usuarios WHERE idUsuario =" + idUsuario, CommandType.Text, ConnectionString.WorkFlow);
                if (numIntento < 2)
                {
                    query = "select top 1 CB, Nombre, DIRECCION, CP, POBLACION, CODREMESA from sicer where IDNOTIFICACION = " + idNotificacion;

                    DataTable dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.Notificaciones);
                    if (dt.Rows.Count > 0)
                    {
                            
                        resp.Append("<campos><campo>");

                        resp.Append("<CB>" + dt.Rows[0]["CB"] + "</CB>");

                        resp.Append("<idNotificacion>" + idNotificacion + "</idNotificacion>");

                        resp.Append("<DIRECCION>" + dt.Rows[0]["DIRECCION"].ToString() + "</DIRECCION>");
                        resp.Append("<CP>" + dt.Rows[0]["CP"] + "</CP>");
                        resp.Append("<POBLACION>" + dt.Rows[0]["POBLACION"] + "</POBLACION>");
                        resp.Append("<REMESA>" + dt.Rows[0]["CODREMESA"] + "</REMESA>");
                        resp.Append("<INTENTO>" + numIntento + "</INTENTO>");
                        resp.Append("<NOMBRE>" + dt.Rows[0]["Nombre"] + "</NOMBRE>");
                            
                        resp.Append("<NOMBRE_USUARIO1>" + nombreUsuario + "</NOMBRE_USUARIO1>");
                        resp.Append("<UBICACION1>" + ubicacion + "</UBICACION1>");
                        resp.Append("<FECHA1>" + fecha + "</FECHA1>");
                        resp.Append("<HORA1>" + hora + "</HORA1>");
                        resp.Append("<RESULTADO1>" + resultado + "</RESULTADO1>");
                        resp.Append("</campo></campos>");
                    }
                }
                else
                {
                    query = "select top 1 CB, Nombre, DIRECCION, CP, POBLACION, CODREMESA, res1, fecha1, hora1, pos1, usuario1  from sicer where IDNOTIFICACION = " + idNotificacion;
                    DataTable dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.Notificaciones);
                    if (dt.Rows.Count > 0)
                    {
                        resp.Append("<campos><campo>");

                        resp.Append("<CB>" + dt.Rows[0]["CB"] + "</CB>");

                        resp.Append("<idNotificacion>" + idNotificacion + "</idNotificacion>");

                        resp.Append("<DIRECCION>" + dt.Rows[0]["DIRECCION"].ToString() + "</DIRECCION>");
                        resp.Append("<CP>" + dt.Rows[0]["CP"] + "</CP>");
                        resp.Append("<POBLACION>" + dt.Rows[0]["POBLACION"] + "</POBLACION>");
                        resp.Append("<REMESA>" + dt.Rows[0]["CODREMESA"] + "</REMESA>");
                        resp.Append("<INTENTO>" + numIntento + "</INTENTO>");
                        resp.Append("<NOMBRE>" + dt.Rows[0]["Nombre"] + "</NOMBRE>");

                        resp.Append("<NOMBRE_USUARIO1>" + dt.Rows[0]["usuario1"] + "</NOMBRE_USUARIO1>");
                        resp.Append("<UBICACION1>" + dt.Rows[0]["pos1"] + "</UBICACION1>");
                        resp.Append("<FECHA1>" + dt.Rows[0]["fecha1"] + "</FECHA1>");
                        resp.Append("<HORA1>" + dt.Rows[0]["hora1"] + "</HORA1>");
                        resp.Append("<RESULTADO1>" + dt.Rows[0]["res1"] + "</RESULTADO1>");

                        resp.Append("<NOMBRE_USUARIO" + numIntento + ">" + nombreUsuario + "</NOMBRE_USUARIO" + numIntento + ">");
                        resp.Append("<UBICACION" + numIntento + ">" + ubicacion + "</UBICACION" + numIntento + ">");
                        resp.Append("<FECHA" + numIntento + ">" + fecha + "</FECHA" + numIntento + ">");
                        resp.Append("<HORA" + numIntento + ">" + hora + "</HORA" + numIntento + ">");
                        resp.Append("<RESULTADO" + numIntento + ">" + resultado + "</RESULTADO" + numIntento + ">");
                        resp.Append("</campo></campos>");
                    }
                }

                return resp.ToString();
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getCamposXMLNotificacion", "wsNotificacionesAppPM", -1, idNotificacion, e.ToString());
                return "-1";
            }
        }

        private int getIdEstado(string idFicha, int numIntento, string estadoNotificacion, string tokenUsuario, string idNotificacion)
        {
            int idEstado = -1;
            try
            {
                string query = "SELECT TOP 1 idEstado FROM Estados WHERE idFlujo = " + idFlujo +
                    " AND idFicha = " + idFicha + " AND FechaPasoFin is null AND FechaPasoInicio is not null";
                DataTable dtDatos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                if (dtDatos.Rows.Count > 0)
                {
                    idEstado = Convert.ToInt32(dtDatos.Rows[0]["idEstado"]);
                }
                else
                {
                    // El flujo parece haber terminado
                    Trazas t = new Trazas("setEstadoNotificaciones", "wsNotificaciones", -1, tokenUsuario + "." + idNotificacion +
                    " . " + numIntento + " . " + estadoNotificacion + " . " + idFlujo + " . " + idFicha, "No hay pasos pendientes para este flujo");
                }

                return idEstado;

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("setEstadoNotificaciones", "wsNotificaciones", -1, tokenUsuario + "." + idNotificacion +
                   " . " + numIntento + " . " + estadoNotificacion, "No hay pasos pendientes para este flujo");
                return idEstado;
            }
        }




        #endregion
    }
}