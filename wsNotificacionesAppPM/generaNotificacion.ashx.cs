﻿using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using workFlow.classes;

namespace wsNotificacionesApp
{
    /// <summary>
    /// Summary description for generaNotificacion
    /// </summary>
    public class generaNotificacion : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string nombre = Utilities.getParam(context, "nombre");
            string direccion = Utilities.getParam(context, "direccion");

            string respuesta = string.Empty;
            byte[] data = null;

            if (string.IsNullOrEmpty(nombre) || string.IsNullOrEmpty(nombre))
            {
                respuesta = "";
            }
            else
            {
                data = File.ReadAllBytes("/inetpub/wwwroot/wsNotificacionesApp/pdf/notificacion.pdf");

                PdfReader pdfReader = new PdfReader(data);

                MemoryStream st = new MemoryStream();

                PdfStamper pdfStamper = new PdfStamper(pdfReader, st);

                AcroFields pdfFormFields = pdfStamper.AcroFields;

                pdfFormFields.SetField("NOMBRE", nombre);
                pdfFormFields.SetField("DIRECCION", direccion);

                // Cambia la propiedad para que no se pueda editar el PDF
                pdfStamper.FormFlattening = true;

                // Cierra el PDF
                pdfStamper.Close();

                pdfReader.Close();

                data = st.ToArray();

            }

            context.Response.ContentType = "application/pdf";
            context.Response.BinaryWrite(data);
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}